CC = gcc
GNATMAKE = gnatmake
GNATCLEAN = gnatclean
RM = rm -f
MKDIR = mkdir -p

default: build

clean:
	$(GNATCLEAN) -P backlit.gpr
#	$(RM) -r adalib adainclude
#	$(RM) -r lib
#	$(RM) -r obj 

rep:
	$(MKDIR) adainclude adalib obj lib

build:	rep
	$(GNATMAKE) -P backlit.gpr

check:	clean rep
	$(GNATMAKE) -gnatv -P backlit.gpr

style:	clean rep
	$(GNATMAKE) -gnaty -P backlit.gpr
