with Ada.Text_IO; use Ada.Text_IO;
with Backlit.Postgres; use Backlit.Postgres;

procedure Simple_Demo is

   use Backlit;

   D,D1,D2 : DBD_Type;

   procedure Create_Film_Table
     (D : in out DBD_Type)
   is
      R : Result_Type;
   begin
      Put_Line ("Create_Film_Table");
      --declare
      --   D : DBD_Type;
      begin
         --  the command can be split in more than one line, in fact
         --  the command will be executed as a single command.
         --  check the documentation for more info.
         D.Append_Command ("DROP TABLE films;");
         D.Exec (R);
      exception
         when Backlit.Query_Error =>
            Put_Line (Error_Message (R));
      end;
      D.Append_Command ("CREATE TABLE films (");
      D.Append_Command (" code        char(5) CONSTRAINT firstkey PRIMARY KEY,");
      D.Append_Command (" title       varchar(40) NOT NULL,");
      D.Append_Command (" did         integer NOT NULL,");
      D.Append_Command (" date_prod   date,");
      D.Append_Command (" kind        varchar(10),");
      D.Append_Command (" len         interval hour to minute);");
      D.Exec (R);
      Put ("Create_Film_Table :");
      if R.Status = COMMAND_OK then
         Put_Line ("Ok");
      else
         Put_Line ("Failed");
      end if;
   exception
      when Backlit.Query_Error =>
         Put_Line (Error_Message (R));
   end Create_Film_Table;

   procedure Insert_Value_1 (D : in out DBD_Type)
   is
      R : Result_Type;
   begin
      D.Append_Command ("INSERT INTO films");
      D.Append_Command ("(code, title, did,date_prod, kind, len)");
      D.Append_Command ("VALUES (1, 'test1', 1, '12-24-2008', 'test', '1h35m');");
      D.Exec (R);
    exception
      when Backlit.Query_Error =>
         Put_Line (Error_Message (R));
   end Insert_Value_1;

   procedure Prepare_1 (D : in out DBD_Type)
   is
   begin
      Put_Line ("prepare_procedure :");
      D.Append_Command ("INSERT INTO films");
      D.Append_Command ("(code, title, did,date_prod, kind, len)");
      D.Append_Command ("VALUES ($1, $2, $3, $4, $5, $6);");
      D.Prepare ("films_insert", 6);
   end Prepare_1;

   procedure Insert_Prepared_1
     (D : in out DBD_Type)
   is
      R : Result_Type;
      Count : Natural := 0;
   begin
      for I in 1 .. 9 loop
         Count := Count + 1;
         Put_Line ("=> code :" & Count'Img);
         D.Bind_Parameter (6, "2h45m");
         D.Bind_Parameter (4, "01-30-2007");
         D.Bind_Parameter (2, "test" & Count'Img);
         D.Bind_Parameter (3, Count'img);
         D.Bind_Parameter (5, "test" & Count'Img);
         D.Bind_Parameter (1, Count'Img);
         --  The bind parameter can be put in any order
         --  thanks to Ada.Containers.Indefinite_Ordered_Maps.
         D.Exec (R,"films_insert");
         -- we insert an error here :
         if Count = 8 then
            Put_Line ("=> We insert an error here : duplicate key");
            Count := 7;
         end if;
      end loop;
   exception
      when Backlit.Query_Error =>
         Put_Line (Error_Message (R));
   end Insert_Prepared_1;

   procedure Insert_Value_2 (D : in out DBD_Type)
   is
      R : Result_Type;
      Code : Natural := 20;
   begin
      Put_Line ("=> We insert directly some value with code 20 named test 20");
      D.Append_Command ("INSERT INTO films");
      D.Append_Command ("(code, title, did,date_prod, kind, len)");
      D.Append_Command ("VALUES ($1, $2, $3, $4, $5, $6);");
      D.Bind_Parameter (6, "2h45m");
      D.Bind_Parameter (4, "01-30-2007");
      D.Bind_Parameter (2, "test 20");
      D.Bind_Parameter (3, "3");
      D.Bind_Parameter (5, "test 20");
      D.Bind_Parameter (1, Code'Img);
      --  The bind parameter can be put in any order
      --  thanks to Ada.Containers.Indefinite_Ordered_Maps.
      D.Exec (R);
    exception
      when Backlit.Query_Error =>
         Put_Line (Error_Message (R));
   end Insert_Value_2;

   procedure select_1 (D : in out DBD_Type)
   is
      R  : Result_Type;
      NT : Tuples_Type;
      NF : Fields_Type;
   begin
      D.Append_Command ("SELECT code, title FROM films");
      D.Append_Command ("WHERE did=$1 OR date_prod > $2;");
      D.Bind_Parameter (1, "1");
      D.Bind_Parameter (2, "01-30-2000");
      D.Exec (R);
      if R.Status = TUPLES_OK then
         NT := R.N_Tuples;
         Put_Line ("number of tuples :" & NT'Img);
         NF := R.N_Fields;
         Put_Line ("number of fields :"& NF'Img);
      end if;
      for I in First_Tuples (R) .. Last_Tuples (R) loop
         Put ('|');
         for J in First_Fields (R) .. Last_Fields (R) loop
            Put (R.Get_Value (I, J));
            Put ('|');
            if J = NF - 1 then
               New_Line;
            end if;
         end loop;
      end loop;

   exception
      when Backlit.Query_Error =>
         Put_Line (Error_Message (R));
   end Select_1;


begin
   ---------------------------
   --  Database Connection  --
   ---------------------------
   ----  there is two way to set the connection
   ----  but you can't use both on the same connection.

   --  D.Set_Host_Name ("localhost");
   --  D.Set_Host_Address ("127.0.0.1");
   --  D.Set_User_Name ("postgres");
   --  D.Set_db_Name ("template1");
   --  D.Set_User_Password ("secret");
   --  D.Set_Port ("5432");
   ----  and then make a server connection
   --  D.Connect;

   --  The other way is to pass the connection parameter
   --  as a string at the connection time
   Put_Line ("stating connection...");
   D.Connect ("host=localhost user=postgres dbname=template1");

   -------------------------
   --  command execution  --
   -------------------------
   if Is_Connected (D) then
      Put ("Connected!");
   else
      Put ("connection failure!");
   end if;
   New_Line;
   --  you may create your own connection procedure
   --  giving the user different try for the password
   --  for example.
   --  check the Needs_Password and Used_Password functions.

   Create_Film_Table (D);
   --  Insert_Value_1 (D);
   D1 := D;
   D1.Connect;
   Prepare_1 (D1);
   Insert_Prepared_1 (D1);
   D2 := D1;
   D2.Connect;
   Insert_Value_2 (D2);
   Select_1 (D);
   New_Line;

exception
   when Backlit.Error =>
      Put_Line (Error_Message (D));
end Simple_Demo;
