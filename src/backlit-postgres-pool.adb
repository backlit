with Ada.Exceptions;
with Interfaces.C.Strings;
with System;
with Ada.Strings.Unbounded;
with Ada.Text_IO; use Ada.Text_IO;

package body Backlit.Postgres.Pool is

   -------------
   --  Equal  --
   -------------
   function "=" (Left, Right : Connection_Type) return Boolean
   is
      use type Backlit.Thin.A_PGCONN_T;
   begin
      if Left.Connection = Right.Connection
        and then Left.Connection /= null then
         return True;
      end if;
      return False;
   end "=";

   ---------------------
   --  Error_Message  --
   ---------------------
   function Error_Message (C : in Connection_Type) return String
   is
   begin
      return Interfaces.C.Strings.Value
        (Backlit.Thin.PQERRORMESSAGE (C.Connection));
   exception
      when others =>
         return "unknow error.";
   end Error_Message;

   --------------------
   --  Is_Connected  --
   --------------------
   function Is_Connected (C : Connection_Type) return Boolean
   is
      use type Backlit.Thin.ConnStatusType;
   begin
      return Backlit.Thin.PQSTATUS (C.Connection) =
        Backlit.Thin.CONNECTION_OK;
   end Is_Connected;

   ---------------
   --  Connect  --
   ---------------
   procedure Connect
     (C : in out Connection_Type)
   is
      CI : Ada.Strings.Unbounded.Unbounded_String;
      use Interfaces.C.Strings;
   begin
      if Is_Connected (C) then
         return;
      end if;
      Connect (C, Ada.Strings.Unbounded.To_String (C.Connection_Info));
   end Connect;

   procedure Connect
     (C : in out Connection_Type; Connection_Info : String)
   is
      CI : Interfaces.C.Strings.chars_ptr;
      use Interfaces.C.Strings;
      use Interfaces.C.Strings;
      use type Backlit.Thin.ConnStatusType;
   begin
      if Is_Connected (C) then
         return;
      end if;
      CI := New_Char_Array (Interfaces.C.To_C (Connection_Info));
      C.Connection := Backlit.Thin.PQCONNECTDB (CI);
      if CI /= Null_Ptr then
         Interfaces.C.Strings.Free (CI);
      end if;
      if not Is_Connected (C) then
         Ada.Exceptions.Raise_Exception (Error'Identity, Error_Message (C));
      end if;
      C.Connection_Info :=
        Ada.Strings.Unbounded.To_Unbounded_String (Connection_Info);
   end Connect;

   ------------------
   --  Initialize  --
   ------------------
   procedure Initialize (C : in out Connection_Type)
   is
   begin
      C.Connection := null;
      C.In_Use := False;
   end Initialize;

--     procedure Adjust (C : in out Connection_Type)
--     is
--     begin
--        null;
--     end Adjust;

--     procedure Finalize (C : in out Connection_Type)
--     is
--     begin
--        null;
--     end Adjust;


   ----------------------
   --  Protected Body  --
   ----------------------
   protected body Session_DBD_Type is

      ---------------
      --  Connect  --
      ---------------
      procedure Connect (L : in out Connection_List_Type) is

         procedure Connect (Cursor : Connection_List.Cursor)
         is
         begin
            Connection_List.Update_Element (L.L, Cursor, Connect'Access);
         end Connect;

         use type Ada.Containers.Count_Type;
         C : Connection_List.Cursor;
      begin
         C := First (L.L);
         loop
            exit when C = Last (L.L);
            Connect (C);
            L.In_Length := L.In_Length + 1;
         end loop;
         --  Connection_List.Iterate (L.L, Connect'Access);
      end Connect;

--      procedure Is_Connect (L : in Connection_List_Type) is

--           procedure  Is_Connected (C : Connection_Type)
--           is
--           begin
--              if Is_Connected (C) then
--                 Put_Line ("Connected");
--              else
--                 Put_Line ("NOT Connected");
--              end if;
--           end Is_Connected;

--           procedure Query (Cursor : Connection_List.Cursor)
--           is
--           begin
--              Query_Element (Cursor, Is_Connected'Access);
--           end Query;
--        begin
--           Connection_List.Iterate (L.L, Query'Access);
--        end Is_Connect;

      ------------
      --  Open  --
      ------------
      procedure Open
        (D : in DBD_Type;
         Key : String;
         Capacity : Positive)
      is
         L : Connection_List_Type;
         Connection_Maps_Position  : Connection_Maps.Cursor;
         Insert_OK : Boolean;
         C_Tab : array (1 .. Capacity) of Connection_Type;
      begin
         L.In_Length := 0;
         L.Out_Length := 0;
         L.L := Empty_List;
         if not Is_Empty (Key) then
            Ada.Exceptions.Raise_Exception
              (Error'Identity, "Open => session already exit");
         end if;
         for I in C_Tab'Range loop
            C_Tab (I).Connection_Info := D.Connection_Info;
            Append (L.L, C_Tab (I));
         end loop;
         if Is_Connected (D) then
            Connect (L);
         end if;
         Insert (Pool.Map, Key, L, Connection_Maps_Position, Insert_OK);
         if Insert_Ok /= True then
            Ada.Exceptions.Raise_Exception
              (Error'Identity, "Open => init failed");
         end if;
         --  Put_Line (Length (L.L)'Img);
      end Open;

      ------------
      --  Open  --
      ------------
      procedure Open
        (Connection_Info : String;
         Key : String;
         Capacity : Positive)
      is
         L : Connection_List_Type;
         Connection_Maps_Position  : Connection_Maps.Cursor;
         Insert_OK : Boolean;
         C_Tab : array (1 .. Capacity) of Connection_Type;
         use type Ada.Containers.Count_Type;
      begin
         L.In_Length := 0;
         L.Out_Length := 0;
         L.L := Empty_List;
         if not Is_Empty (Key) then
            Ada.Exceptions.Raise_Exception
              (Error'Identity, "Open => session already exit");
         end if;
         for I in C_Tab'Range loop
            Connect (C_Tab (I), Connection_Info);
            C_Tab (I).Connection_Info :=
              Ada.Strings.Unbounded.To_Unbounded_String (Connection_Info);
            Append (L.L, C_Tab (I));
            L.In_Length := L.In_Length + 1;
            C_Tab (I).Connection := null;
         end loop;
         Insert (Pool.Map, Key, L, Connection_Maps_Position, Insert_OK);
         if Insert_Ok /= True then
            Ada.Exceptions.Raise_Exception
              (Error'Identity, "Open => init failed");
         end if;
         --  Connect (L);
      end Open;

      -----------
      --  Get  --
      -----------
      procedure Get
        (D : out  DBD_Type;
         Key : String;
         Reserve : Positive)
      is
         procedure Get1 (Key : String; Element : in out Connection_List_Type)
         is
            Element_Found : Boolean := False;
            procedure Get2 (Cursor : Connection_List.Cursor)
            is
               procedure Get3 (C : in out  Connection_Type)
               is
                  use type Ada.Containers.Count_Type;
               begin
                  if C.In_Use = False then
                     D.Real := C.Connection;
                     D.Connection_Info := C.Connection_Info;
                     C.In_Use := True;
                     Element_Found := True;
                     Element.Out_Length := Element.Out_Length + 1;
                     Element.In_Length := Element.In_Length - 1;
                  end if;
               end Get3;
            begin
               Connection_List.Update_Element (Element.L, Cursor, Get3'Access);
            end Get2;
            C : Connection_List.Cursor := First (Element.L);
         begin
            loop
               exit when Element_Found or C = Last (Element.L);
               Get2 (C);
               Next (C);
            end loop;
         end Get1;
      begin
         --  Put_Line ("POOL => Get");
         D.Init;
         if Contains (Pool.Map, Key) then
            Connection_Maps.Update_Element
              (Pool.Map, Find (Pool.Map, Key), Get1'Access);
            D.Pooled := True;
            D.Key := Ada.Strings.Unbounded.To_Unbounded_String (Key);
         else
            Ada.Exceptions.Raise_Exception
              (Error'Identity, "Get => session '" & Key & "' not found");
         end if;
      end Get;

      -----------
      --  Put  --
      -----------
      procedure Put
        (D : in out DBD_Type;
         Key : String)
      is
         procedure Put1 (Key : String; Element : in out Connection_List_Type)
         is
            Element_Found : Boolean := False;
            procedure Put2 (Cursor : Connection_List.Cursor)
            is
               procedure Put3 (C : in out  Connection_Type)
               is
                  use type System.Address;
                  use type Backlit.Thin.A_PGCONN_T;
                  use type Ada.Containers.Count_Type;
               begin
                  if C.In_Use = True and then
                    Is_Connected (D) then
                     --  D.Real = C.Connection then
                     if D.Real.all'Address = C.Connection.all'Address then
                        D.Real := null;
                        C.In_Use := False;
                        Element_Found := True;
                        Element.In_Length := Element.In_Length + 1;
                        Element.Out_Length := Element.Out_Length - 1;
                     end if;
                  end if;
               end Put3;
            begin
               --  if not Is_Empty (Element) then
               Connection_List.Update_Element (Element.L, Cursor, Put3'Access);
               --  end if;
            end Put2;
            C : Connection_List.Cursor;
         begin
            if Is_Empty (Element.L) /= True then
               C := First (Element.L);
               loop
                  exit when Element_Found or C = Last (Element.L);
                  Put2 (C);
                  Next (C);
               end loop;
            end if;
         end Put1;
      begin
         if D.Pooled /= True then
            Ada.Exceptions.Raise_Exception
              (Error'Identity, "Put => not a valid connection");
         end if;
         if Contains (Pool.Map, Key) then
            Connection_Maps.Update_Element
              (Pool.Map, Find (Pool.Map, Key), Put1'Access);
         end if;
      exception
         when Error =>
            raise;
      end Put;

      -------------
      --  Close  --
      -------------
      procedure Close (Key : String)
      is
         procedure Close (Key : String; L : in out Connection_List_Type) is
            procedure Close (Cursor : Connection_List.Cursor)
            is
               procedure Close  (C : in out  Connection_Type)
               is
                  use type Backlit.Thin.A_PGCONN_T;
               begin
                  if C.Connection /= null then
                     Backlit.Thin.PQFINISH (C.Connection);
                     C.Connection := null;
                  end if;
               end Close;
            begin
               Connection_List.Update_Element (L.L, Cursor, Close'Access);
            end Close;
         begin
            Connection_List.Iterate (L.L, Close'Access);
         end Close;
      begin
         if Contains (Pool.Map, Key) then
            Pool.Map.Update_Element (Pool.Map.Find (Key), Close'Access);
         end if;
      exception
         when Error =>
            raise;
      end Close;

      --------------
      --  Delete  --
      --------------
      procedure Delete (Key : String)
      is
      begin
         Exclude (Pool.Map, Key);
      end Delete;

      -------------
      --  Close  --
      -------------
      procedure Close
      is
         procedure Close (C : Connection_Maps.Cursor)
         is
         begin
            Close (Key (C));
         end Close;
      begin
         Pool.Map.Iterate (Close'Access);
         Clear (Pool.Map);
      exception
         when Error =>
            raise;
      end Close;

      function Is_Empty (Key : String) return Boolean
      is
      begin
         if Contains (Pool.Map, Key) then
            return Connection_List.Is_Empty (Element (Pool.Map.Find (Key)).L);
         end if;
         return True;
      end Is_Empty;

      function Total_Length (Key : String) return Count_Type
      is
      begin
         return Connection_List.Length (Element (Pool.Map.Find (Key)).L);
      end Total_Length;

      function In_Length (Key : String) return Count_Type
      is
      begin
         return Element (Pool.Map.Find (Key)).In_Length;
      end In_Length;

      function Out_Length (Key : String) return Count_Type
      is
      begin
         return Element (Pool.Map.Find (Key)).Out_Length;
      end Out_Length;

   end Session_DBD_Type;

   --      if Length (L) < 2 then
   --              declare
   --                 D_Tab : array (1 .. Reserve) of DBD_Type :=
   --                   (others => First_Element (L));
   --              begin
   --                 for I in D_Tab'First .. D_Tab'Last loop
   --                    Connect (D_Tab(I));
   --                    Append (L, D_Tab(I));
   --                 end loop;
   --              end;
   --           end if;

   ------------------------
   --  Public Interface  --
   ------------------------

   procedure Open
     (D : in DBD_Type;
      Key : String;
      Capacity : Positive := 3)
   is
   begin
      Session_DBD_Type.Open (D, Key, Capacity);
   end Open;

   procedure Open
     (Connection_Info : String;
      Key : String;
      Capacity : Positive := 3)
   is
   begin
      Session_DBD_Type.Open (Connection_Info, Key, Capacity);
   end Open;

   procedure Get
     (D : out  DBD_Type;
      Key : String;
      Reserve : Positive := 2)
   is
   begin
      Session_DBD_Type.Get (D, Key, Reserve);
   end Get;

   procedure Put
     (D : in out DBD_Type;
      Key : String)
   is
   begin
      Session_DBD_Type.Put (D, Key);
   end Put;

   procedure Close
     (Key : String)
   is
   begin
      Session_DBD_Type.Close (Key);
      Session_DBD_Type.Delete (Key);
   end Close;

   procedure Close
   is
   begin
      Session_DBD_Type.Close;
   end Close;

   function Is_Empty (Key : String) return Boolean
   is
   begin
      return Session_DBD_Type.Is_Empty (Key);
   end Is_Empty;

   function Total_Length (Key : String) return Count_Type
   is
   begin
      return Session_DBD_Type.Total_Length (Key);
   end Total_Length;

   function In_Length (Key : String) return Count_Type
   is
   begin
      return Session_DBD_Type.In_Length (Key);
   end In_Length;

   function Out_Length (Key : String) return Count_Type
   is
   begin
      return Session_DBD_Type.Out_Length (Key);
   end Out_Length;

end Backlit.Postgres.Pool;
