private with Ada.Containers.Doubly_Linked_Lists;
private with Ada.Containers.Indefinite_Hashed_Maps;
private with Ada.Strings.Hash;
private with Ada.Strings.Unbounded;
with Ada.Containers;

package Backlit.Postgres.Pool is

  --  Error : exception;

   subtype Count_Type is Ada.Containers.Count_Type;

   procedure Open
     (D : in DBD_Type;
      Key : String;
      Capacity : Positive := 3);
   --  create a new pool containing "Capacity" DBD_type referenced by Key
   --  with the connection information provided by D.
   --  If D is connected then all the pool will be connected.
   --  D is left untouched so you may reuse or leave it.
   --  Opening an existing session referenced by Key will raise an exception.

   procedure Open
     (Connection_Info : String;
      Key : String;
      Capacity : Positive := 3);
   --  create a new pool containing "Capacity" DBD_type referenced by Key
   --  with the connection information provided by the Connection_Info.
   --  All the DBD_Type will attempt a connection.
   --  Opening an existing session referenced by Key will raise an exception.

   procedure Get
     (D : out  DBD_Type;
      Key : String;
      Reserve : Positive := 2);
   --  get a new DBD_Type referenced by Key with all the connection
   --  information initialised previously and/or
   --  already connected to the database server.

   procedure Put
     (D : in out DBD_Type;
      Key : String);
   --  The Connection embeded in D, will be put back in the pool
   --  if found, otherwise the connection in D will be close
   --  when D is finalized.
   --  This procedure is called when D is Finalized.
   --  An exception is raised if D doesn't come from the pool.

   procedure Close
     (Key : String);
   --  release all the connection refenced by the Key.
   --  do nothing if Key is not found.

   procedure Close;
   --  close the pool of connections.

   function Is_Empty (Key : String) return Boolean;
   --  Return True if the Key is not found or Contains no connections
   --  at all.

   function Total_Length (Key : String) return Count_Type;
   --  Return the total number of connection per key.

   function In_Length (Key : String) return Count_Type;
   --  Return the number of connection ready to be used.
   --  (never out or put back via the put procedure or finalization)

   function Out_Length (Key : String) return Count_Type;
   --  Return the number of connection already in use
   --  (going out with the get procedure)

private

   type Connection_Type is new
     Ada.Finalization.Controlled with
      record
         Connection       : Backlit.Thin.A_PGCONN_T;
         Connection_Info  : Ada.Strings.Unbounded.Unbounded_String;
         In_Use           : Boolean;
      end record;

   procedure Initialize (C : in out Connection_Type);
   --  procedure Adjust     (C : in out Connection_Type);
   --  procedure Finalize   (C : in out Connection_Type);

   function "=" (Left, Right : Connection_Type) return Boolean;

   function Is_Connected
     (C : Connection_Type) return Boolean;

   procedure Connect
     (C : in out Connection_Type);

   procedure Connect
     (C : in out Connection_Type;
      Connection_Info : String);


   package Connection_List is new Ada.Containers.Doubly_Linked_Lists
     (Element_Type => Connection_Type,
      "=" => "=");
   use Connection_List;

   type Connection_List_Type is
      record
         L : Connection_List.List;
         In_Length  : Count_Type;
         Out_Length : Count_Type;
      end record;

   package Connection_Maps is
      new Ada.Containers.Indefinite_Hashed_Maps
     (Key_Type        => String,
      Element_Type    => Connection_List_Type,
      Hash            => Ada.Strings.Hash,
      Equivalent_Keys => "=");
   use Connection_Maps;

   type  DBD_Map_Type is new
     Ada.Finalization.Limited_Controlled with
      record
         Map : Connection_Maps.Map;
      end record;

   protected Session_DBD_Type is

      procedure Open
        (D : in DBD_Type;
         Key : String;
         Capacity : Positive);

      procedure Open
        (Connection_Info : String;
         Key : String;
         Capacity : Positive);

      procedure Get
        (D : out  DBD_Type;
         Key : String;
         Reserve : Positive);

      procedure Put
        (D : in out DBD_Type;
         Key : String);

      procedure Close
        (Key : String);

      procedure Delete
        (Key : String);

      procedure Close;

      function Is_Empty
        (Key : String) return Boolean;

      function Total_Length
        (Key : String) return Count_Type;

      function In_Length
        (Key : String) return Count_Type;

      function Out_Length
        (Key : String) return Count_Type;

   private
      Pool : DBD_Map_Type;

   end Session_DBD_Type;

end Backlit.Postgres.Pool;
