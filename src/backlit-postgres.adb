------------------------------------------------------------------------------
--                                                                          --
-- Copyright 2008, Ali Bendriss                                             --
--                                                                          --
-- This file is part of Backlit.                                            --
--                                                                          --
-- Backlit is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU Lesser General Public License as published by       --
-- the Free Software Foundation, either version 3 of the License, or        --
-- (at your option) any later version.                                      --
--                                                                          --
-- Backlit is distributed in the hope that it will be useful,               --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of           --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the             --
-- GNU Lesser General Public License for more details.                      --
--                                                                          --
-- You should have received a copy of the GNU Lesser General Public License --
-- along with Backlit. If not, see <http://www.gnu.org/licenses/>.          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Exceptions;
with Interfaces.C.Strings;
with Ada.Strings.Unbounded;
with Ada.Characters.Handling;
with Backlit.Postgres.Pool;
--  with Ada.Text_IO; use Ada.Text_IO;

package body Backlit.Postgres is

   use Connection_Info_Map;
   use Backlit.Thin;
   use Ada.Exceptions;

   -------------------------
   --                     --
   -------------------------
   procedure Connection_Map_To_Unbounded_String
     (C : in out DBD_Type;
      I : out Ada.Strings.Unbounded.Unbounded_String)
   is
      use Ada.Strings.Unbounded;
      use Interfaces.C;
      use Interfaces.C.Strings;
      TMP_US : Unbounded_String;
      procedure Append_Info (C : Connection_Info_Map.Cursor) is
      begin
         Ada.Strings.Unbounded.Append
           (TMP_US, Connection_Info_Map.Element (C) & " ");
      end Append_Info;
   begin
      Connection_Info_Map.Iterate (C.Map, Append_Info'Access);
      --Connection_Info_Map.Clear (C.Map);
      I := TMP_US;
   end Connection_Map_To_Unbounded_String;
   pragma Inline (Connection_Map_To_Unbounded_String);

   procedure To_C_Binded_Parameter_Value
     (S : DBD_Type;
      O : out Pg_ParamValues) is
      use Interfaces.C;
      use type Ada.Containers.Count_Type;

      Map_Length : Positive := Natural (Stm_Map.Length (S.Parameter));
      Index : size_t;
   begin
      if Stm_Map.Last_Key (S.Parameter) /= S.Param_Length then
         Raise_Exception (Error'Identity, "wrong bind key parameter");
      end if;
      for I in 1 .. Map_Length loop
         Index := size_t (I - 1);
         O (Index) := Strings.New_Char_Array
           (To_C (Stm_Map.Element (S.Parameter, I)));
      end loop;
   end To_C_Binded_Parameter_Value;
   pragma Inline (To_C_Binded_Parameter_Value);

   --------------------------
   --  Connection Control  --
   --------------------------

   procedure Set_Host_Name
     (D : in out DBD_Type; Value : String)
   is
   begin
      D.Map.Insert ("host", "host=" & Value);
   end Set_Host_Name;

   procedure Set_Host_Address
     (D : in out DBD_Type; Value : String)
   is
   begin
      D.Map.Insert ("hostaddr", "hostaddr=" & Value);
   end Set_Host_Address;

   procedure Set_Port
     (D : in out DBD_Type; Value : String)
   is
   begin
      D.Map.Insert ("port", "port=" & Value);
   end Set_Port;

   procedure Set_Db_Name
     (D : in out DBD_Type; Value : String)
   is
   begin
      D.Map.Insert ("dbname", "dbname=" & Value);
   end Set_Db_Name;

   procedure Set_User_Name
     (D : in out DBD_Type; Value : String)
   is
   begin
      D.Map.Insert ("user", "user=" & Value);
   end Set_User_Name;

   procedure Set_User_Password
     (D : in out DBD_Type; Value : String)
   is
   begin
      D.Map.Insert ("password", "password=" & Value);
   end Set_User_Password;

   procedure Set_Timeout
     (D : in out DBD_Type; Value : Natural)
   is
   begin
      D.Map.Insert ("connect_timeout", "connect_timeout=" & Value'Img);
   end Set_Timeout;

   procedure Set_Options
     (D : in out DBD_Type; Value : String)
   is
   begin
      D.Map.Insert ("options", "options=" & Value);
   end Set_Options;

   procedure Set_SSL
     (D : in out DBD_Type; Value : SSL_Mode_Type)
   is

      function PQ_Value (SSL : SSL_Mode_Type) return String
      is
      begin
         case SSL is
            when Disable => return "disable";
            when Allow   => return "allow";
            when Prefer  => return "prefer";
            when Require => return "require";
         end case;
      end PQ_Value;
   begin
      D.Map.Insert ("sslmode", "sslmode=" & PQ_Value (Value));
   end Set_SSL;

   procedure Set_Krbsrvname
     (D : in out DBD_Type; Value : String)
   is
   begin
      D.Map.Insert ("krbsrvname", "options=" & Value);
   end Set_Krbsrvname;

   procedure Set_Gssapi
     (D : in out DBD_Type)
   is
   begin
      D.Map.Insert ("gsslib", "gsslib=gssapi");
   end Set_Gssapi;

   procedure Set_Service_Name
     (D : in out DBD_Type; Value : String)
   is
   begin
      D.Map.Insert ("service", "service=" & Value);
   end Set_Service_Name;

   procedure Connect
     (D : in out DBD_Type)
   is
      CI : Ada.Strings.Unbounded.Unbounded_String;
      use Interfaces.C.Strings;
   begin
      if Is_Connected (D) then
         return;
      end if;
      if Is_Empty (D.Map) then
         Connect (D, Ada.Strings.Unbounded.To_String (D.Connection_Info));
      else
         Connection_Map_To_Unbounded_String (D, CI);
         Connect (D, Ada.Strings.Unbounded.To_String (CI));
      end if;
   end Connect;

   procedure Connect
     (D : in out DBD_Type; Connection_Info : String)
   is
      CI : Interfaces.C.Strings.chars_ptr;
      use Interfaces.C.Strings;
      use Interfaces.C.Strings;
   begin
      if Is_Connected (D) then
         return;
      end if;

      CI := New_Char_Array (Interfaces.C.To_C (Connection_Info));
      D.Real := Backlit.Thin.PQCONNECTDB (CI);
      if CI /= Null_Ptr then
         Interfaces.C.Strings.Free (CI);
      end if;
      if PQSTATUS (D.Real) /= CONNECTION_OK then
         Raise_Exception (Error'Identity, Error_Message (D));
      end if;
      D.Connection_Info :=
        Ada.Strings.Unbounded.To_Unbounded_String (Connection_Info);
   end Connect;

   procedure Reset
     (D : in out DBD_Type)
   is
   begin
      PQRESET (D.Real);
      if PQSTATUS (D.Real) /= CONNECTION_OK then
         Raise_Exception (Error'Identity, Error_Message (D));
      end if;
   end Reset;

   procedure Connect_Non_Blocking
     (D : in out DBD_Type)
   is
   begin
      pragma Assert (False, "Connect_Non_Blocking not implemented");
      null;
   end Connect_Non_Blocking;

   procedure Reset_Non_Blocking
     (D : in out DBD_Type)
   is
   begin
      pragma Assert (False, "Reset_Non_Blocking not implemented");
      null;
   end Reset_Non_Blocking;

   -------------------------
   --  Connection Status  --
   -------------------------

   function Get
     (D : DBD_Type;
      Q : Connection_Key_Type) return String
   is
   begin
      case Q is
         when DB_Name =>
            return Interfaces.C.Strings.Value (PQDB (D.Real));
         when DB_User =>
            return Interfaces.C.Strings.Value (PQUSER (D.Real));
         when DB_Pass =>
            return Interfaces.C.Strings.Value (PQPASS (D.Real));
         when DB_Host =>
            return Interfaces.C.Strings.Value (PQHOST (D.Real));
         when DB_Port =>
            return Interfaces.C.Strings.Value (PQPORT (D.Real));
         when DB_Options =>
            return Interfaces.C.Strings.Value (PQOPTIONS (D.Real));
      end case;
   end Get;

   function Error_Message (D : in DBD_Type) return String
   is
   begin
      return Interfaces.C.Strings.Value (PQERRORMESSAGE (D.Real));
   exception
      when others =>
         return "unrecognized error";
   end Error_Message;

   function Current_Server_Setting
     (D        : DBD_Type;
      Key      : String;
      To_Lower : Boolean := True) return String
   is
      Param    : Interfaces.C.Strings.chars_ptr;
      Real_Key : String := Key;
   begin
      if To_Lower = True then
         Real_Key := Ada.Characters.Handling.To_Lower (Key);
      end if;
      Param := Interfaces.C.Strings.New_Char_Array
        (Interfaces.C.To_C (Real_Key));
      declare
         Result : String := Interfaces.C.Strings.Value
           (PQPARAMETERSTATUS (D.Real, Param));
      begin
         Interfaces.C.Strings.Free (Param);
         return Result;
      end;
   exception
      when others => return "";
   end Current_Server_Setting;

   function Needs_Password
     (D : DBD_Type) return Boolean
   is
      use type Interfaces.C.int;
   begin
      return PQCONNECTIONNEEDSPASSWORD (D.Real) = C_True;
   end Needs_Password;

   function Used_Password
     (D : DBD_Type) return Boolean
   is
      use type Interfaces.C.int;
   begin
      return PQCONNECTIONUSEDPASSWORD (D.Real) = C_True;
   end Used_Password;

   function Is_Connected
     (D : DBD_Type) return Boolean
   is
   begin
      return PQSTATUS (D.Real) = CONNECTION_OK;
   end Is_Connected;

   -------------------------
   --  Command Execution  --
   -------------------------


   procedure Append_Command
     (D : in out DBD_Type;
      Q : String)
   is
      use Ada.Strings.Unbounded;
   begin
      D.Command := D.Command & Q & " ";
   end Append_Command;

   procedure Bind_Parameter
     (D     : in out DBD_Type;
      Key   : Positive;
      Value : String)
   is
      use type Interfaces.C.int;
   begin
      Stm_Map.Insert (D.Parameter, Key, Value);
      D.Param_Length := D.Param_Length + 1;
   end Bind_Parameter;

   procedure Exec
     (D : in out DBD_Type;
      R : out Result_Type'Class)
   is
      use Interfaces.C;
      use type Interfaces.C.Strings.chars_ptr;
      use type Thin.ExecStatusType;
      use type Thin.A_PGRESULT_T;
      Command    : Strings.chars_ptr;
   begin
      Command := Strings.New_Char_Array
        (To_C (Ada.Strings.Unbounded.To_String (D.Command)));
      if Stm_Map.Is_Empty (D.Parameter) then
         --  exec without parameters
         R.Pg_Result :=  Thin.PQEXEC (D.Real, Command);
      else
         --  exec with parameters
         declare
            Pg_Param   : Pg_ParamValues (0 .. size_t (D.Param_Length - 1)) :=
              (others => Interfaces.C.Strings.Null_Ptr);
         begin
            To_C_Binded_Parameter_Value (D, Pg_Param);
            R.Pg_Result :=  Thin.PQEXECPARAMS
              (D.Real, Command, int (D.Param_Length),
               null, Pg_Param (0)'Access, null, null, 0);
            for I in Pg_Param'Range loop
               Interfaces.C.Strings.Free (Pg_Param (I));
            end loop;
         end;
      end if;

      if Command /= Strings.Null_Ptr then
         Interfaces.C.Strings.Free (Command);
      end if;

      Stm_Map.Clear (D.Parameter);
      D.Param_Length := 0;
      D.Command := Ada.Strings.Unbounded.Null_Unbounded_String;

      if R.Pg_Result = null then
         Raise_Exception (Error'Identity, Error_Message (D));
         --  S.Pg_Result so we use the last Error message available
         --  in the connection
      end if;
      if Thin.PQRESULTSTATUS (R.Pg_Result) /= Thin.PGRES_COMMAND_OK and then
        Thin.PQRESULTSTATUS (R.Pg_Result) /= Thin.PGRES_TUPLES_OK then
         --  FIXME test that rules with command returning data
         --  and command returning no data.
         Raise_Exception (Query_Error'Identity, Error_Message (R));
      end if;
   end Exec;

   procedure Prepare
     (D                   : in out DBD_Type;
      Prepared_Stm_Name   : String  := "";
      Number_Of_Parameter : Natural := 0)
   is
      use Interfaces.C;
      use type Interfaces.C.Strings.chars_ptr;
      use type Thin.ExecStatusType;
      use type Thin.A_PGRESULT_T;
      Command    : Strings.chars_ptr;
      Stm_Name   : Strings.chars_ptr;
      NParams    : constant int := int (Number_Of_Parameter);
      R : Result_Type;
   begin
      Command := Strings.New_Char_Array
        (To_C (Ada.Strings.Unbounded.To_String (D.Command)));
      Stm_Name := Strings.New_Char_Array
        (To_C (Prepared_Stm_Name));

      R.Pg_Result := Thin.PQPREPARE (D.Real, Stm_Name, Command, NParams, null);

      D.Command := Ada.Strings.Unbounded.Null_Unbounded_String;

      if Command /= Strings.Null_Ptr then
         Interfaces.C.Strings.Free (Command);
      end if;
      if Stm_Name /= Strings.Null_Ptr then
         Interfaces.C.Strings.Free (Stm_Name);
      end if;

      if R.Pg_Result = null then
         Raise_Exception (Error'Identity, Error_Message (D));
         --  S.Pg_Result so we use the last Error message available
         --  in the connection
      end if;
      if Thin.PQRESULTSTATUS (R.Pg_Result) /= Thin.PGRES_COMMAND_OK then
         Raise_Exception (Query_Error'Identity, Error_Message (R));
      end if;
   end Prepare;

   procedure Exec
     (D : in out DBD_Type;
      R : out Result_Type'Class;
      Prepared_Stm_Name   : String)
   is
      use Interfaces.C;
      use type Interfaces.C.Strings.chars_ptr;
      use type Thin.ExecStatusType;
      use type Thin.A_PGRESULT_T;
      Stm_Name   : Strings.chars_ptr;
      Param_Last : Natural := D.Param_Length;
   begin
      Stm_Name := Strings.New_Char_Array
        (To_C (Prepared_Stm_Name));
      if Param_Last > 0 then
         Param_Last := Param_Last - 1;
      end if;
      declare
         Pg_Param   : Pg_ParamValues (0 .. size_t (Param_Last)) :=
           (others => Interfaces.C.Strings.Null_Ptr);
      begin
         if D.Param_Length > 0 then
            To_C_Binded_Parameter_Value (D, Pg_Param);
         end if;
         R.Pg_Result :=  Thin.PQEXECPREPARED
           (D.Real, Stm_Name, int (D.Param_Length),
            Pg_Param (0)'Access, null, null, 0);
         for I in Pg_Param'Range loop
            Interfaces.C.Strings.Free (Pg_Param (I));
         end loop;
      end;
      Stm_Map.Clear (D.Parameter);
      D.Param_Length := 0;
      if Stm_Name /= Strings.Null_Ptr then
         Interfaces.C.Strings.Free (Stm_Name);
      end if;
      if R.Pg_Result = null then
         Raise_Exception (Error'Identity, Error_Message (D));
         --  S.Pg_Result so we use the last Error message available
         --  in the connection
      end if;
      if Thin.PQRESULTSTATUS (R.Pg_Result) /= Thin.PGRES_COMMAND_OK then
         Raise_Exception (Query_Error'Identity, Error_Message (R));
      end if;
   end Exec;

   function Error_Message (R : Result_Type) return String
   is
   begin
      return Interfaces.C.Strings.Value
        (Thin.PQRESULTERRORMESSAGE (R.Pg_Result));
   exception
      when others =>
         return "unknow error.";
   end Error_Message;

   function Status (R : Result_Type) return Exec_Status_Type
   is
      C_Result : ExecStatusType;
   begin
      C_Result := Thin.PQRESULTSTATUS (R.Pg_Result);
      return Exec_Status_Type'Val (Thin.ExecStatusType'Pos (C_Result));
   end Status;

   -------------------------------------------
   --  Retrieving Query Result Information  --
   -------------------------------------------


   function N_Tuples (R : Result_Type) return Tuples_Type
   is
      Result : Tuples_Type :=
        Tuples_Type (Thin.PQNTUPLES (R.Pg_Result));
   begin
      return Result;
   end N_Tuples;

   function First_Tuples (R : Result_Type) return Tuples_Type
   is
   begin
      return 0;
   end First_Tuples;

   function Last_Tuples (R : Result_Type) return Tuples_Type
   is
   begin
      return  N_Tuples (R) - 1;
   end Last_Tuples;


   function N_Fields (R : Result_Type) return Fields_Type
   is
      Result : Fields_Type :=
        Fields_Type (Thin.PQNFIELDS (R.Pg_Result));
   begin
      return Result;
   end N_Fields;

   function First_Fields (R : Result_Type) return Fields_Type
   is
   begin
      return 0;
   end First_Fields;

   function Last_Fields (R : Result_Type) return Fields_Type
   is
   begin
      return  N_Fields (R) - 1;
   end Last_Fields;

   function F_Name
     (R             : Result_Type;
      Column_Number : Fields_Type)
     return String
   is
      use Interfaces.C;
      Result : String := Interfaces.C.Strings.Value
        (Thin.PQFNAME (R.Pg_Result, int (Column_Number)));
   begin
      return Result;
   end F_Name;

   function F_Number
     (R           : Result_Type;
      Column_Name : String)
     return Fields_Type
   is
      use Interfaces.C;
      use type Interfaces.C.Strings.chars_ptr;
      C_Column_Name : Strings.chars_ptr :=
        Strings.New_Char_Array (To_C (Column_Name));
      Result : Fields_Type :=
        Fields_Type (Thin.PQFNUMBER (R.Pg_Result, C_Column_Name));
   begin
      if C_Column_Name /= Strings.Null_Ptr then
         Interfaces.C.Strings.Free (C_Column_Name);
      end if;
      return Result;
   end F_Number;


   function F_Table
     (R             : Result_Type;
      Column_Number : Fields_Type) return Oid
   is
      use Interfaces.C;
      Result : Oid :=
        Oid (Thin.PQFTABLE (R.Pg_Result, int (Column_Number)));
   begin
      return Result;
   end F_Table;

   function F_Table_Col
     (R : Result_Type;
      Column_Number : Fields_Type) return Fields_Type
   is
      use Interfaces.C;
      Result : Fields_Type :=
        Fields_Type (Thin.PQFTABLECOL (R.Pg_Result, int (Column_Number)));
   begin
      return Result;
   end F_Table_Col;

   function F_Format
     (R             : Result_Type;
      Column_Number : Fields_Type) return  Format_Code_Type
   is
      use Interfaces.C;
      Result : Format_Code_Type :=
        Format_Code_Type (Thin.PQFFORMAT (R.Pg_Result, int (Column_Number)));
   begin
      return Result;
   end F_Format;

   function F_Type
     (R             : Result_Type;
      Column_Number : Fields_Type) return Oid
   is
      use Interfaces.C;
      Result : Oid :=
        Oid (Thin.PQFTYPE (R.Pg_Result, int (Column_Number)));
   begin
      return Result;
   end F_Type;

   function F_Mod
     (R             : Result_Type;
      Column_Number : Fields_Type) return Modifier_Type
   is
      use Interfaces.C;
      Result : Modifier_Type :=
        Modifier_Type (Thin.PQFMOD (R.Pg_Result, int (Column_Number)));
   begin
      return Result;
   end F_Mod;

   function F_Size
     (R             : Result_Type;
      Column_Number : Fields_Type) return Size_Type
   is
      use Interfaces.C;
      Result : Size_Type :=
        Size_Type (Thin.PQFSIZE (R.Pg_Result, int (Column_Number)));
   begin
      return Result;
   end F_Size;

   function Get_Value
     (R             : Result_Type;
      Row_Number    : Tuples_Type;
      Column_Number : Fields_Type) return String
   is
      use Interfaces.C;
      use type Interfaces.C.Strings.chars_ptr;
      C_Result : Strings.chars_ptr :=
        Thin.PQGETVALUE (R.Pg_Result,
                         int (Row_Number),
                         int (Column_Number));
      Result : String := Strings.Value (C_Result);
      --  The caller should not free the result directly.
      --  It will be freed when the associated PGresult handle
      --  is passed to PQclear.
   begin
      return Result;
   end Get_Value;

   function Get_Is_Null
     (R             : Result_Type;
      Row_Number    : Tuples_Type;
      Column_Number : Fields_Type) return Boolean
   is
      use Interfaces.C;
      C_Result : int := Thin.PQGETISNULL (R.Pg_Result,
                                          int (Row_Number),
                                          int (Column_Number));
   begin
      return C_Result = Thin.C_True;
   end Get_Is_Null;

   function Getlength
     (R             : Result_Type;
      Row_Number    : Tuples_Type;
      Column_Number : Fields_Type) return Length_Type
   is
      use Interfaces.C;
      Result : Length_Type :=
        Length_Type (Thin.PQGETLENGTH (R.Pg_Result,
                                       int (Row_Number),
                                       int (Column_Number)));
   begin
      return Result;
   end Getlength;

   function N_Params (R : Result_Type) return Param_Number_Type
   is
      use Interfaces.C;
      Result : Param_Number_Type :=
        Param_Number_Type (Thin.PQNPARAMS (R.Pg_Result));
   begin
      return Result;
   end N_Params;

   function Param_Type
     (R            : Result_Type;
      Param_Number : Param_Number_Type) return Oid
   is
      use Interfaces.C;
      Result : Oid :=
        Oid (Thin.PQPARAMTYPE (R.Pg_Result, int (Param_Number)));
   begin
      return Result;
   end Param_Type;

   ----------------
   --  private   --
   ----------------

   procedure Initialize (D : in out  DBD_Type)
   is
   begin
      --  Put_Line ("Initialize");
      D.Real := null;
      Connection_Info_Map.Clear (D.Map);
      D.Command := Ada.Strings.Unbounded.Null_Unbounded_String;
      Stm_Map.Clear (D.Parameter);
      D.Param_Length := 0;
      D.Connection_Info := Ada.Strings.Unbounded.Null_Unbounded_String;
      D.Pooled := False;
      D.Key := Ada.Strings.Unbounded.Null_Unbounded_String;

      --  D.Command := Ada.Strings.Unbounded.Null_Unbounded_String;
      --  Stm_Map.Clear (D.Parameter);
      --  D.Param_Length := 0;
      --  Put_Line ("Initialized");
   end Initialize;

   procedure Adjust    (D : in out  DBD_Type)
   is
      Connection_Info : String :=
        Ada.Strings.Unbounded.To_String (D.Connection_Info);
   begin
      --  Put_Line ("Adjust");
      D.Real := null;
      D.Pooled := False;
      --  Connect (D, Connection_Info);
      --  Put_Line ("Adjusted");
   end Adjust;

   procedure Finalize  (D : in out  DBD_Type)
   is
   begin
      --  Put_Line ("Finalize");
      Stm_Map.Clear (D.Parameter);
      D.Param_Length := 0;
      if D.Real /= null then
         if D.Pooled then
            Backlit.Postgres.Pool.Put
              (D,Ada.Strings.Unbounded.To_String (D.Key));
         else
            PQFINISH (D.Real);
            D.Real := null;
         end if;
      end if;
      --  Put_Line ("Finalized");
   end Finalize;

   procedure Initalize (R : in out  Result_Type)
   is
   begin
      R.Pg_Result := null;
   end Initalize;

   procedure Finalize  (R : in out  Result_Type)
   is
   begin
      if R.Pg_Result /= null then
         Thin.PQCLEAR (R.Pg_Result);
         R.Pg_Result := null;
      end if;
   end Finalize;

end Backlit.Postgres;
