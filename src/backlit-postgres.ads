------------------------------------------------------------------------------
--                                                                          --
-- Copyright 2008, Ali Bendriss                                             --
--                                                                          --
-- This file is part of Backlit.                                            --
--                                                                          --
-- Backlit is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU Lesser General Public License as published by       --
-- the Free Software Foundation, either version 3 of the License, or        --
-- (at your option) any later version.                                      --
--                                                                          --
-- Backlit is distributed in the hope that it will be useful,               --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of           --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the             --
-- GNU Lesser General Public License for more details.                      --
--                                                                          --
-- You should have received a copy of the GNU Lesser General Public License --
-- along with Backlit. If not, see <http://www.gnu.org/licenses/>.          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Finalization;
private with Ada.Containers.Indefinite_Hashed_Maps;
private with Ada.Strings.Hash;
private with Ada.Strings.Unbounded;
private with Ada.Containers.Indefinite_Ordered_Maps;
private with Interfaces.C;
private with Interfaces.C.Strings;

with Backlit.Thin;

package Backlit.Postgres is

   type DBD_Type is new
     Ada.Finalization.controlled with private;

   type Result_Type is new
     Ada.Finalization.Limited_Controlled with private;

   --------------------------
   --  Connection Control  --
   --------------------------

   procedure Set_Host_Name
     (D : in out DBD_Type; Value : String);
   --  Name of Host To Connect To.
   --  If this Begins with a slash, it specifies Unix-Domain Communication
   --  rather than TCP/IP communication. The Value is the Name of the Directory
   --  in which the socket file is stored.
   --  The default behavior when host is not specified is to connect to
   --  A Unix-Domain Socket in /tmp (or whatever socket directory was specified
   --  when PostgreSQL was built).
   --  On machines without Unix-Domain Sockets,
   --  the default is to connect to localhost.

   procedure Set_Host_Address
     (D : in out DBD_Type; Value : String);
   --  Numeric IP address of host to connect to.
   --  This should be in the standard IPv4 address format, e.g., 172.28.40.9.
   --  if your machine supports IPv6, you can also use those addresses.
   --  TCP/IP communication is always used when a nonempty string
   --  is specified for this parameter.

   procedure Set_Port
     (D : in out DBD_Type; Value : String);
   --  Port number to connect to at the server host,
   --  or socket file name extension for Unix-Domain Connections.

   procedure Set_Db_Name
     (D : in out DBD_Type; Value : String);
   --  The database name. Defaults to be the same as the user name.

   procedure Set_User_Name
     (D : in out DBD_Type; Value : String);
   --  PostgreSQL user name to connect as.
   --  Defaults to be the same as the operating system name of the user
   --  running the application.

   procedure Set_User_Password
     (D : in out DBD_Type; Value : String);
   --  Password to be used if the server demands password authentication.

   procedure Set_Timeout
     (D : in out DBD_Type; Value : Natural);
   --  Maximum wait for connection, in seconds (write as a Decimal).
   --  Zero or not specified means wait indefinitely.
   --  It is not recommended to use a timeout of less than 2 seconds.

   procedure Set_Options
     (D : in out DBD_Type; Value : String);
   --  Command-line options to be sent to the server.

   type SSL_Mode_Type is (Disable, Allow, Prefer, Require);

   procedure Set_SSL
     (D : in out DBD_Type; Value : SSL_Mode_Type);
   --  This option determines whether or
   --  with what priority an SSL Connection will be Negotiated
   --  with the server. There are four modes:
   --  * Disable will attempt only an unencrypted SSL Connection;
   --  * Allow will negotiate, trying first a non-SSL connection,
   --  then if that fails, trying an SSL connection;
   --  * Prefer (the default) will negotiate, trying first an SSL connection,
   --  then if that fails, trying a regular non-SSL connection;
   --  * Require will try only an SSL connection.
   --  if PostgreSQL is compiled without SSL support,
   --  using option Require will cause an error,
   --  while options Allow and Prefer will be accepted but
   --  will not in fact attempt an SSL connection.

   procedure Set_Krbsrvname
     (D : in out DBD_Type; Value : String);
   --  Kerberos service name to use when authenticating with
   --  Kerberos 5 or GSSAPI.
   --  This must match the service name specified in the server
   --  configuration for Kerberos authentication to Succeed.

   procedure Set_Gssapi
     (D : in out DBD_Type);
   --  Only Used On Windows.
   --  Set_Gssapi force the use the GSSAPI library for Authentication
   --  instead of the default SSPI.

   procedure Set_Service_Name
     (D : in out DBD_Type; Value : String);
   --  Service name to use for additional parameters.
   --  It specifies a service name in pg_service.conf that holds additional
   --  connection parameters. This allows applications to Specify only a
   --  service name so connection parameters can be centrally maintained.

   procedure Connect
     (D : in out DBD_Type);
   --  Makes a new connection to the database server.
   --  return if already connected.

   procedure Connect
     (D : in out DBD_Type; Connection_Info : String);
   --  Makes a new connection to the database server.
   --  Take the connection info from a string like
   --  "host=localhost user=postgres dbname=template1 ..."
   --  rather than using the procedure Set_Host_Name or Set_Host_Address.
   --  You can't mix the two ways on the same connection.
   --  return if already connected.

   procedure Reset
     (D : in out DBD_Type);
   --  This procedure will close the connection to the server and
   --  attempt to reestablish a new connection to the same server,
   --  using all the same parameters previously used.
   --  This might be useful for error recovery if a working connection is lost.


   procedure Connect_Non_Blocking
     (D : in out DBD_Type);
   --  Make a connection to the database server in a nonblocking manner.

   procedure Reset_Non_Blocking
     (D : in out DBD_Type);
   --  This procedure will close the connection to the server and
   --  attempt to reestablish a new connection to the same server,
   --  using all the same parameters previously used.
   --  This can be useful for error recovery if a working connection is lost.
   --  It differ from reset (above) in that it act in a nonblocking manner.


   -------------------------
   --  Connection Status  --
   -------------------------
   type Connection_Key_Type is
     (DB_Name, DB_User, DB_Pass, DB_Host, DB_Port, DB_Options);

   function Get
     (D : DBD_Type;
      Q : Connection_Key_Type) return String;
   --  The following functions return parameter values established at
   --  connection. These values are fixed for the life of the PGconn object.

   function Error_Message (D : in DBD_Type) return String;
   --  Returns the error message most recently generated by
   --  an operation on the connection.

   function Current_Server_Setting
     (D        : DBD_Type;
      Key      : String;
      To_Lower : Boolean := True) return String;
   --  Certain parameter values are reported by the server automatically
   --  at connection startup or whenever their values change.
   --  this function can be used to interrogate these settings.
   --  It returns the current value of a parameter if known,
   --  or an empty string if the parameter is not known.
   --  key looks like : server_version, server_encoding...
   --  check the doc for Parameters reported as of the current release.

   function Needs_Password
     (D : DBD_Type) return Boolean;
   --  Returns true if the connection authentication method
   --  required a password, but none was available.
   --  This function can be applied after a failed connection attempt
   --  to decide whether to prompt the user for a password.

   function Used_Password
     (D : DBD_Type) return Boolean;
   --  This function detects whether a password supplied to the connection
   --  function was actually used. Passwords obtained from other sources
   --  (such as the .pgpass file) are not considered caller-supplied.

   function Is_Connected
     (D : DBD_Type) return Boolean;
   --  return True is the connection status is OK.


   -------------------------
   --  Command Execution  --
   -------------------------


   procedure Append_Command
     (D : in out DBD_Type;
      Q : String);
   --  The SQL command string to be executed.
   --  If parameters are used,
   --  they are referred to in the command string as $1, $2, etc.
   --  And the coresponding Keys in the Bind_Parameter procedure
   --  are referred as 1, 2, etc

   procedure Bind_Parameter
     (D     : in out DBD_Type;
      Key   : Positive;
      Value : String);
   --  If paramaters a set then exec will run Thin.PQexecParams
   --  rather than Thin.Exec. The primary advantage of PQexecParams over PQexec
   --  is that parameter values can be separated from the command string,
   --  thus avoiding the need for tedious and error-prone quoting and escaping.
   --
   --  Unlike PQexec, PQexecParams allows at most one SQL command in
   --  the given string. (There can be semicolons in it, but
   --  not more than one nonempty command.) This is a limitation
   --  of the underlying protocol, but has some usefulness as an extra defense
   --  against SQL-injection attacks.

   procedure Exec
     (D : in out DBD_Type;
      R : out Result_Type'Class);
   --  Submits a command to the server and waits for the result,
   --  with the ability to pass parameters separately
   --  from the SQL command text by using the Bind_Parameter procedure

   procedure Prepare
     (D                   : in out DBD_Type;
      Prepared_Stm_Name   : String  := "";
      Number_Of_Parameter : Natural := 0);
   --  Creates a prepared statement named Prepared_Stm_Name
   --  from the query string supplied by the Append_Command procedure,
   --  which must contain a single SQL command. Prepared_Stm_Name can be "" to
   --  create an unnamed statement, in which case any pre-existing unnamed
   --  statement is automatically replaced; otherwise it is an error if the
   --  statement name is already defined in the current session.
   --  If any parameters are used, they are referred to in the query
   --  as $1, $2, etc. Number_Of_Parameter is their number.

   procedure Exec
     (D : in out DBD_Type;
      R : out Result_Type'Class;
      Prepared_Stm_Name   : String);
   --  Same as the Exec procedure but take a prepared statment name
   --  as parameter to run a prepared statment using Prepare.

   function Error_Message (R : Result_Type) return String;
   --  Returns the error message associated with the command,
   --  or an empty string if there was no error.


   type Exec_Status_Type is
     (EMPTY_QUERY,
      --  The string sent to the server was empty.
      COMMAND_OK,
      --  Successful completion of a command returning no data.
      TUPLES_OK,
      --  Successful completion of a command returning data
      --  (such as a SELECT or SHOW).
      COPY_OUT,
      --  Copy Out (from server) data transfer started.
      COPY_IN,
      --  Copy In (to server) data transfer started.
      BAD_RESPONSE,
      --  The server's response was not understood.
      NONFATAL_ERROR,
      --  A nonfatal error (a notice or warning) occurred.
      FATAL_ERROR
      --  A fatal error occurred.
     );

   function Status (R : Result_Type) return Exec_Status_Type;
   --  If the result status is PGRES_TUPLES_OK, then the functions described
   --   below can be used to retrieve the rows returned by the query.
   --  Note that a SELECT command that happens to retrieve zero rows
   --  still shows PGRES_TUPLES_OK. PGRES_COMMAND_OK is for commands that
   --  can never return rows (INSERT, UPDATE, etc.).
   --  A response of PGRES_EMPTY_QUERY might indicate a bug
   --  in the client software. A result of status PGRES_NONFATAL_ERROR
   --  will never be returned directly by PQexec or other query execution
   --  functions; results of this kind are instead passed to the
   --  notice processor

   -------------------------------------------
   --  Retrieving Query Result Information  --
   -------------------------------------------

   subtype Oid is Thin.Oid range Thin.Oid'Range;

   InvalidOid  : constant Oid := 0;

   type Tuples_Type is new Integer;

   function N_Tuples (R : Result_Type) return Tuples_Type;
   --  Returns the number of rows (tuples) in the query result.
   --  Because it returns an integer result,
   --  large result sets might overflow the return value on 32-bit
   --  operating systems.
   --  FIXME change it for Long_integer for 64 Bit os ??? (in thin as well)

   function First_Tuples (R : Result_Type) return Tuples_Type;

   function Last_Tuples (R : Result_Type) return Tuples_Type;
   --  Returns the First/Last row (tuples) in the query result.
   --  May be used to loop :
   --  for First_Tuples (R) .. Last_Tuples (R) loop
   --   ...
   --  end loop;

   type Fields_Type is new Integer;

   function N_Fields (R : Result_Type) return Fields_Type;
   --  Returns the number of columns (fields) in each row of the query result.

   function First_Fields (R:  Result_Type) return Fields_Type;

   function Last_Fields (R:  Result_Type) return Fields_Type;
   --  Returns the First/Last columns (fields) of the query result.
   --  May be used to loop :
   --  for First_Fields (R) .. Last_Fields (R) loop
   --   ...
   --  end loop;

   function F_Name
     (R             : Result_Type;
      Column_Number : Fields_Type)
     return String;
   --  Returns the column name associated with the given column number.
   --  Column numbers start at 0.

   function F_Number
     (R           : Result_Type;
      Column_Name : String)
     return Fields_Type;
   --  Returns the column number associated with the given column name.
   --  -1 is returned if the given name does not match any column.
   --  The given name is treated like an identifier in an SQL command,
   --  that is, it is downcased unless double-quoted.

   function F_Table
     (R             : Result_Type;
      Column_Number : Fields_Type) return Oid;
   --  Returns the OID of the table from which the given column was fetched.
   --  Column numbers start at 0.
   --  InvalidOid is returned if the column number is out of range,
   --  or if the specified column is not a simple reference to a table column,
   --  or when using pre-3.0 protocol. You can query the system table pg_class
   --  to determine exactly which table is referenced.

   function F_Table_Col
     (R : Result_Type;
      Column_Number : Fields_Type) return Fields_Type;
   --  Returns the column number (within its table) of the column making up the
   --  specified query result column. Query-result column numbers start at 0,
   --  but table columns have nonzero numbers.
   --  Zero is returned if the column number is out of range,
   --  or if the specified column is not a simple reference to a table column,
   --  or when using pre-3.0 protocol.

   type Format_Code_Type is new Integer;

   function F_Format
     (R             : Result_Type;
      Column_Number : Fields_Type) return  Format_Code_Type;
   --  Returns the format code indicating the format of the given column.
   --  Column numbers start at 0. Format code zero indicates textual data
   --  representation, while format code one indicates binary representation.
   --  (Other codes are reserved for future definition.)

   function F_Type
     (R             : Result_Type;
      Column_Number : Fields_Type) return Oid;
   --  Returns the data type associated with the given column number.
   --  The value returned is the internal OID number of the type.
   --  Column numbers start at 0. You can query the system table pg_type to
   --  obtain the names and properties of the various data types.
   --  The OIDs of the built-in data types are defined in the file
   --  src/include/catalog/pg_type.h in the source tree.

   type Modifier_Type is new Integer;

   function F_Mod
     (R             : Result_Type;
      Column_Number : Fields_Type) return Modifier_Type;
   --  Returns the type modifier of the column associated with the
   --  given column number. Column numbers start at 0. The interpretation
   --  of modifier values is type-specific;
   --  they typically indicate precision or size limits.
   --  The value -1 is used to indicate "no information available".
   --  Most data types do not use modifiers,
   --  in which case the value is always -1.

   type Size_Type is new Integer;

   function F_Size
     (R             : Result_Type;
      Column_Number : Fields_Type) return Size_Type;
   --  Returns the size in bytes of the column associated with the given
   --  column number. Column numbers start at 0. Fsize returns the space
   --  allocated for this column in a database row, in other words the size
   --  of the server's internal representation of the data type.
   --  (Accordingly, it is not really very useful to clients.)
   --  A negative value indicates the data type is variable-length.

   function Get_Value
     (R             : Result_Type;
      Row_Number    : Tuples_Type;
      Column_Number : Fields_Type) return String;
   --  Returns a single field value of one row of a PGresult.
   --  Row and column numbers start at 0.

   function Get_Is_Null
     (R             : Result_Type;
      Row_Number    : Tuples_Type;
      Column_Number : Fields_Type) return Boolean;
   --  Tests a field for a null value. Row and column numbers start at 0.

   type Length_Type is new Integer;
   function Getlength
     (R             : Result_Type;
      Row_Number    : Tuples_Type;
      Column_Number : Fields_Type) return Length_Type;
   --  Returns the actual length of a field value in bytes.
   --  Row and column numbers start at 0.

   type Param_Number_Type is new Integer range 0 .. Integer'Last;

   function N_Params (R : Result_Type) return Param_Number_Type;
   --  Returns the number of parameters of a prepared statement.
   --  This function is only useful when inspecting the result of
   --  PQdescribePrepared. For other types of queries it will return zero.

   function Param_Type
     (R            : Result_Type;
      Param_Number : Param_Number_Type) return Oid;
   --  Returns the data type of the indicated statement parameter.
   --  Parameter numbers start at 0.
   --  This function is only useful when inspecting the result of
   --  PQdescribePrepared. For other types of queries it will return zero.

private

   package Connection_Info_Map is
      new Ada.Containers.Indefinite_Hashed_Maps
     (Key_Type        => String,
      Element_Type    => String,
      Hash            => Ada.Strings.Hash,
      Equivalent_Keys => "=");

   package Stm_Map is new  Ada.Containers.Indefinite_Ordered_Maps
     (Key_Type => Positive,
      Element_Type => String,
      "<" => "<",
      "=" => "=");

   type DBD_Type is new
     Ada.Finalization.Controlled with
      record
         Real            : Backlit.Thin.A_PGCONN_T;
         Map             : Connection_Info_Map.Map;
         Command         : Ada.Strings.Unbounded.Unbounded_String;
         Parameter       : Stm_Map.Map;
         Param_Length    : Natural;
         Connection_Info : Ada.Strings.Unbounded.Unbounded_String;
         Pooled          : Boolean;
         Key             : Ada.Strings.Unbounded.Unbounded_String;
      end record;

   procedure Initialize (D : in out  DBD_Type);
   procedure Adjust    (D : in out  DBD_Type);
   procedure Finalize  (D : in out  DBD_Type);

   procedure Init (D : in out  DBD_Type) renames Initialize;
   pragma Inline (Init);

   type Result_Type is new
     Ada.Finalization.Limited_Controlled with
      record
         Pg_Result    : Thin.A_PGRESULT_T;
      end record;

   procedure Initalize (R : in out  Result_Type);

   procedure Finalize  (R : in out  Result_Type);

   type Pg_ParamTypes is array (Interfaces.C.size_t range <>)
     of aliased Thin.Oid;
   pragma Convention (C, Pg_ParamTypes);

   type Pg_ParamValues is array (Interfaces.C.size_t range <>)
     of aliased Interfaces.C.Strings.chars_ptr;
   pragma Convention (C, Pg_ParamValues);

   type ParamLengths is array (Interfaces.C.size_t range <>)
     of aliased Interfaces.C.int;
   pragma Convention (C, ParamLengths);

   type ParamFormats is array (Interfaces.C.size_t range <>)
     of aliased Interfaces.C.int;
   pragma Convention (C, ParamFormats);

   type Pg_Parameter_Type is record
      ParamTypes   : Thin.Oid := Thin.InvalidOid;
      ParamValues  : Interfaces.C.Strings.chars_ptr;
      ParamLengths : Interfaces.C.Int;
      ParamFormats : Interfaces.C.int := 0;
   end record;
   --  only the Pg_ParamValues will be used for now as we are processing
   --  text-format parameters.

end Backlit.Postgres;
