------------------------------------------------------------------------------
--                                                                          --
-- Copyright 2008, Ali Bendriss                                             --
--                                                                          --
-- This file is part of Backlit.                                            --
--                                                                          --
-- Backlit is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU Lesser General Public License as published by       --
-- the Free Software Foundation, either version 3 of the License, or        --
-- (at your option) any later version.                                      --
--                                                                          --
-- Backlit is distributed in the hope that it will be useful,               --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of           --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the             --
-- GNU Lesser General Public License for more details.                      --
--                                                                          --
-- You should have received a copy of the GNU Lesser General Public License --
-- along with Backlit. If not, see <http://www.gnu.org/licenses/>.          --
--                                                                          --
------------------------------------------------------------------------------

with System;
with Interfaces.C;
--  with Interfaces.C.Extensions;
with Interfaces.C.Strings;
with Interfaces.C_Streams;

package Backlit.Thin is

   pragma Preelaborate;

   C_True  : constant Interfaces.C.int := 1;
   C_False : constant Interfaces.C.int := 0;

   type ConnStatusType is
     (CONNECTION_OK,                                       --  libpq-fe.h:40
      CONNECTION_BAD,                                      --  libpq-fe.h:41
      CONNECTION_STARTED,                                  --  libpq-fe.h:48
      CONNECTION_MADE,                                     --  libpq-fe.h:49
      CONNECTION_AWAITING_RESPONSE,                        --  libpq-fe.h:50
      CONNECTION_AUTH_OK,                                  --  libpq-fe.h:52
      CONNECTION_SETENV,                                   --  libpq-fe.h:54
      CONNECTION_SSL_STARTUP,                              --  libpq-fe.h:55
      CONNECTION_NEEDED                                    --  libpq-fe.h:57
     );
   for ConnStatusType'Size use 32;
   pragma Convention (C, ConnStatusType);

   type PostgresPollingStatusType is
     (PGRES_POLLING_FAILED,                                --  libpq-fe.h:61
      PGRES_POLLING_READING,                               --  libpq-fe.h:62
      PGRES_POLLING_WRITING,                               --  libpq-fe.h:63
      PGRES_POLLING_OK,                                    --  libpq-fe.h:64
      PGRES_POLLING_ACTIVE
     );
   for PostgresPollingStatusType'Size use 32;
   pragma Convention (C, PostgresPollingStatusType);

   type ExecStatusType is
     (PGRES_EMPTY_QUERY,                                   --  libpq-fe.h:71
      PGRES_COMMAND_OK,                                    --  libpq-fe.h:72
      PGRES_TUPLES_OK,                                     --  libpq-fe.h:75
      PGRES_COPY_OUT,                                      --  libpq-fe.h:78
      PGRES_COPY_IN,                                       --  libpq-fe.h:79
      PGRES_BAD_RESPONSE,                                  --  libpq-fe.h:80
      PGRES_NONFATAL_ERROR,                                --  libpq-fe.h:82
      PGRES_FATAL_ERROR                                    --  libpq-fe.h:84
     );
   for ExecStatusType'Size use 32;
   pragma Convention (C, ExecStatusType);

   type PGTRANSACTIONSTATUSTYPE is
     (PQTRANS_IDLE,                                        --  libpq-fe.h:88
      PQTRANS_ACTIVE,                                      --  libpq-fe.h:89
      PQTRANS_INTRANS,                                     --  libpq-fe.h:90
      PQTRANS_INERROR,                                     --  libpq-fe.h:91
      PQTRANS_UNKNOWN                                      --  libpq-fe.h:93
     );
   for PGTRANSACTIONSTATUSTYPE'Size use 32;
   pragma Convention (C, PGTRANSACTIONSTATUSTYPE);

   type PGVERBOSITY is
     (PQERRORS_TERSE,                                      --  libpq-fe.h:97
      PQERRORS_DEFAULT,                                    --  libpq-fe.h:98
      PQERRORS_VERBOSE                                     --  libpq-fe.h:100
     );
   for PGVERBOSITY'Size use 32;
   pragma Convention (C, PGVERBOSITY);

   type Oid is new Interfaces.C.unsigned;                 --  postgres_ext.h:29
   --  Object ID is a fundamental type in Postgres.
   InvalidOid  : constant Oid := 0;                       --  postgres_ext.h:34

   --  type PGCONN_T is new Interfaces.C.Extensions.opaque_structure_def;
   type PGCONN_T is new System.Address;
   --  libpq-fe.h:105

   type A_PGCONN_T is access all PGCONN_T;
   --  libpq-fe.h:204
   --  pragma Convention (C, A_PGCONN_T);

   --  type PGRESULT_T is new Interfaces.C.Extensions.opaque_structure_def;
   type PGRESULT_T is new System.Address;
   --  libpq-fe.h:112

   type A_PGRESULT_T is access all PGRESULT_T;
   --  libpq-fe.h:136

   --  type PGCANCEL_T is new Interfaces.C.Extensions.opaque_structure_def;
   type PGCANCEL_T is new System.Address;
   --  libpq-fe.h:118

   type A_PGCANCEL_T is access all PGCANCEL_T;
   --  libpq-fe.h:238
   --  pragma Convention (C, A_PGCANCEL_T);

   type PGNOTIFY_T;
   type A_PGNOTIFY_T is access all PGNOTIFY_T;             --  libpq-fe.h:132
   --  pragma Convention (C, A_PGNOTIFY_T);

   type PGNOTIFY_T is
      record
         relname : Interfaces.C.Strings.chars_ptr;          --  libpq-fe.h:128
         be_pid  : Interfaces.C.int;                        --  libpq-fe.h:129
         extra   : Interfaces.C.Strings.chars_ptr;          --  libpq-fe.h:130
         next    : A_PGNOTIFY_T;                            --  libpq-fe.h:132
      end record;
   pragma Convention (C, PGNOTIFY_T);

   type PQNOTICERECEIVER_T is access procedure
     (arg : System.Address;
      res : A_PGRESULT_T);                                  --  libpq-fe.h:136
   pragma Convention (C, PQNOTICERECEIVER_T);

   type PQNOTICEPROCESSOR_T is access procedure
     (arg     : System.Address;
      message : Interfaces.C.Strings.chars_ptr);            --  libpq-fe.h:137
   pragma Convention (C, PQNOTICEPROCESSOR_T);

   type pqbool is new Interfaces.C.signed_char range 0 .. 1; --  libpq-fe.h:140
   for pqbool'Size use Interfaces.C.signed_char'Size;

   type PQPRINTOPT_T  is                                   --  libpq-fe.h:142
      record
         header    : pqbool;                                --  libpq-fe.h:144
         align     : pqbool;                                --  libpq-fe.h:145
         standard  : pqbool;                                --  libpq-fe.h:146
         html3     : pqbool;                                --  libpq-fe.h:147
         expanded  : pqbool;                                --  libpq-fe.h:148
         pager     : pqbool;                                --  libpq-fe.h:149
         fieldSep  : Interfaces.C.Strings.chars_ptr;        --  libpq-fe.h:150
         tableOpt  : Interfaces.C.Strings.chars_ptr;        --  libpq-fe.h:151
         caption   : Interfaces.C.Strings.chars_ptr;        --  libpq-fe.h:152
         fieldName : access Interfaces.C.Strings.chars_ptr; --  libpq-fe.h:153
      end record;
   pragma Convention (C, PQPRINTOPT_T);

   type A_PQPRINTOPT_T is access constant PQPRINTOPT_T;     -- libpq-fe.h:463
   pragma Convention (C, A_PQPRINTOPT_T);

   type PQCONNINFOOPTION_T is                               --  libpq-fe.h:165
      record
         keyword  : Interfaces.C.Strings.chars_ptr;         --  libpq-fe.h:167
         envvar   : Interfaces.C.Strings.chars_ptr;         --  libpq-fe.h:168
         compiled : Interfaces.C.Strings.chars_ptr;         --  libpq-fe.h:169
         val      : Interfaces.C.Strings.chars_ptr;         --  libpq-fe.h:170
         label    : Interfaces.C.Strings.chars_ptr;         --  libpq-fe.h:171
         dispchar : Interfaces.C.Strings.chars_ptr;         --  libpq-fe.h:172
         dispsize : Interfaces.C.int;                       --  libpq-fe.h:177
      end record;
   pragma Convention (C, PQCONNINFOOPTION_T);

   type A_PQCONNINFOOPTION_T is access all PQCONNINFOOPTION_T;
   --  libpq-fe.h:221
   pragma Convention (C, A_PQCONNINFOOPTION_T);

   type union_anonymous5_t_kind is                         --  libpq-fe.h:192
     (ptr_kind, integer_kind);

   type union_anonymous5_t                                 --  libpq-fe.h:192
     (Which : union_anonymous5_t_kind := ptr_kind)
      is
      record
         case Which is
            when ptr_kind =>
               ptr : access Interfaces.C.int;               --  libpq-fe.h:190
            when integer_kind =>
               integer : Interfaces.C.int;                  --  libpq-fe.h:191
         end case;
      end record;

   pragma Convention (C, union_anonymous5_t);
   pragma Unchecked_Union (union_anonymous5_t);

   type PQARGBLOCK_T is record
      len   : Interfaces.C.int;                             --  libpq-fe.h:186
      isint : Interfaces.C.int;                             --  libpq-fe.h:187
      u     : union_anonymous5_t;                           --  libpq-fe.h:192
   end record;
   pragma Convention (C, PQARGBLOCK_T);

   type A_PQARGBLOCK_T is access constant PQARGBLOCK_T;    -- libpq-fe.h:386

   type pgthreadlock_t is access procedure
     (acquire : Interfaces.C.int);                          --  libpq-fe.h:300
   pragma Convention (C, pgthreadlock_t);

   ---------------------------------------------------
   --  make a new client connection to the backend  --
   ---------------------------------------------------

   --  Asynchronous (non-blocking)

   function PQCONNECTSTART
     (conninfo : Interfaces.C.Strings.chars_ptr)
     return A_PGCONN_T;
   --  libpq-fe.h:204

   function PQCONNECTPOLL
     (conn : A_PGCONN_T)
     return PostgresPollingStatusType;
   --  libpq-fe.h:205

   --  Synchronous (blocking)

   function PQCONNECTDB
     (conninfo : Interfaces.C.Strings.chars_ptr)
     return A_PGCONN_T;
   --  libpq-fe.h:208

   function PQSETDBLOGIN
     (pghost    : Interfaces.C.Strings.chars_ptr;
      pgport    : Interfaces.C.Strings.chars_ptr;
      pgoptions : Interfaces.C.Strings.chars_ptr;
      pgtty     : Interfaces.C.Strings.chars_ptr;
      dbName    : Interfaces.C.Strings.chars_ptr;
      login     : Interfaces.C.Strings.chars_ptr;
      pwd       : Interfaces.C.Strings.chars_ptr)
     return A_PGCONN_T;
   --  libpq-fe.h:209


   procedure PQFINISH
     (conn : A_PGCONN_T);
   --  libpq-fe.h:218
   --  close the current connection and
   --  free the PGconn data structure

   function PQCONNDEFAULTS
     return A_PQCONNINFOOPTION_T;
   --  libpq-fe.h:221
   --  get info about connection options known to PQconnectdb

   procedure PQCONNINFOFREE
     (connOptions : A_PQCONNINFOOPTION_T);
   --  libpq-fe.h:224
   --  free the data structure returned by PQconndefaults()

   -----------------------------------------------------
   --  close the current connection and               --
   --  restablish a new one with the same parameters  --
   -----------------------------------------------------

   --  Asynchronous (non-blocking)

   function PQRESETSTART
     (conn : A_PGCONN_T)
     return Interfaces.C.int;
   --  libpq-fe.h:231

   function PQRESETPOLL
     (conn : A_PGCONN_T)
     return PostgresPollingStatusType;
   --  libpq-fe.h:232

   --  Synchronous (blocking)

   procedure PQRESET
     (conn : A_PGCONN_T);
   --  libpq-fe.h:235


   function PQGETCANCEL
     (conn : A_PGCONN_T)
     return A_PGCANCEL_T;
   --  libpq-fe.h:238
   --  request a cancel structure

   procedure PQFREECANCEL
     (cancel : A_PGCANCEL_T);
   --  libpq-fe.h:241
   --  free a cancel structure

   function PQCANCEL
     (cancel     : A_PGCANCEL_T;
      errbuf     : Interfaces.C.Strings.chars_ptr;
      errbufsize : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:244
   --  issue a cancel request

   function PQREQUESTCANCEL
     (conn : A_PGCONN_T)
     return Interfaces.C.int;
   --  libpq-fe.h:247
   --  backwards compatible version of PQcancel;
   --  not thread-safe

   ---------------------------------------------
   --  Accessor functions for PGconn objects  --
   ---------------------------------------------
   function PQDB
     (conn : A_PGCONN_T)
     return Interfaces.C.Strings.chars_ptr;
   --  libpq-fe.h:250

   function PQUSER
     (conn : A_PGCONN_T)
     return Interfaces.C.Strings.chars_ptr;
   --  libpq-fe.h:251

   function PQPASS
     (conn : A_PGCONN_T)
     return Interfaces.C.Strings.chars_ptr;
   --  libpq-fe.h:252

   function PQHOST
     (conn : A_PGCONN_T)
     return Interfaces.C.Strings.chars_ptr;
   --  libpq-fe.h:253

   function PQPORT
     (conn : A_PGCONN_T)
     return Interfaces.C.Strings.chars_ptr;
   --  libpq-fe.h:254

   function PQTTY
     (conn : A_PGCONN_T)
     return Interfaces.C.Strings.chars_ptr;
   --  libpq-fe.h:255

   function PQOPTIONS
     (conn : A_PGCONN_T)
     return Interfaces.C.Strings.chars_ptr;
   --  libpq-fe.h:256

   function PQSTATUS
     (conn : A_PGCONN_T)
     return ConnStatusType;
   --  libpq-fe.h:257

   function PQTRANSACTIONSTATUS
     (conn : A_PGCONN_T)
     return PGTRANSACTIONSTATUSTYPE;
   --  libpq-fe.h:258

   function PQPARAMETERSTATUS
     (conn      : A_PGCONN_T;
      paramName : Interfaces.C.Strings.chars_ptr)
     return Interfaces.C.Strings.chars_ptr;
   --  libpq-fe.h:259

   function PQPROTOCOLVERSION
     (conn : A_PGCONN_T)
     return Interfaces.C.int;
   --  libpq-fe.h:261

   function PQSERVERVERSION
     (conn : A_PGCONN_T)
     return Interfaces.C.int;
   --  libpq-fe.h:262

   function PQERRORMESSAGE
     (conn : A_PGCONN_T)
     return Interfaces.C.Strings.chars_ptr;
   --  libpq-fe.h:263

   function PQSOCKET
     (conn : A_PGCONN_T)
     return Interfaces.C.int;
   --  libpq-fe.h:264

   function PQBACKENDPID
     (conn : A_PGCONN_T)
     return Interfaces.C.int;
   --  libpq-fe.h:265

   function PQCONNECTIONNEEDSPASSWORD
     (conn : A_PGCONN_T)
     return Interfaces.C.int;
   --  libpq-fe.h:266

   function PQCONNECTIONUSEDPASSWORD
     (conn : A_PGCONN_T)
     return Interfaces.C.int;
   --  libpq-fe.h:267

   function PQCLIENTENCODING
     (conn : A_PGCONN_T)
     return Interfaces.C.int;
   --  libpq-fe.h:268

   function PQSETCLIENTENCODING
     (conn     : A_PGCONN_T;
      encoding : Interfaces.C.Strings.chars_ptr)
     return Interfaces.C.int;
   --  libpq-fe.h:269

   function PQGETSSL
     (conn : A_PGCONN_T)
     return System.Address;
   --  libpq-fe.h:273
   --  Get the OpenSSL structure associated with a connection.
   --  Returns NULL for unencrypted connections or
   --  if any other TLS library is in use.

   procedure PQINITSSL
     (do_init : Interfaces.C.int);
   --  libpq-fe.h:276
   --  Tell libpq whether it needs to initialize OpenSSL

   function PQSETERRORVERBOSITY
     (conn      : A_PGCONN_T;
      verbosity : PGVERBOSITY)
     return PGVERBOSITY;
   --  libpq-fe.h:279
   --  Set verbosity for PQerrorMessage and PQresultErrorMessage

   procedure PQTRACE
     (conn       : A_PGCONN_T;
      debug_port : Interfaces.C_Streams.FILEs);
   --  libpq-fe.h:282
   --  Enable tracing

   procedure PQUNTRACE
     (conn : A_PGCONN_T);
   --  libpq-fe.h:283
   --  Disable tracing

   function PQSETNOTICERECEIVER
     (conn : A_PGCONN_T;
      proc : PQNOTICERECEIVER_T;
      arg  : System.Address)
     return PQNOTICERECEIVER_T;
   --  libpq-fe.h:286
   --  Override default notice handling routines

   function PQSETNOTICEPROCESSOR
     (conn : A_PGCONN_T;
      proc : PQNOTICEPROCESSOR_T;
      arg  : System.Address)
     return PQNOTICEPROCESSOR_T;
   --  libpq-fe.h:289
   --  Override default notice handling routines

   function PQREGISTERTHREADLOCK
     (newhandler : pgthreadlock_t)
     return pgthreadlock_t;
   --  libpq-fe.h:302
   --  Used to set callback that prevents concurrent access to
   --  non-thread safe functions that libpq needs.
   --  The default implementation uses a libpq internal mutex.
   --  Only required for multithreaded apps that use kerberos
   --  both within their app and for postgresql connections.

   --------------------------------
   --  Simple synchronous query  --
   --------------------------------

   function PQEXEC
     (conn  : A_PGCONN_T;
      query : Interfaces.C.Strings.chars_ptr)
     return A_PGRESULT_T;
   --  libpq-fe.h:307

   function PQEXECPARAMS
     (conn         : A_PGCONN_T;
      command      : Interfaces.C.Strings.chars_ptr;
      nParams      : Interfaces.C.int;
      paramTypes   : access Oid;
      paramValues  : access Interfaces.C.Strings.chars_ptr;
      paramLengths : access Interfaces.C.int;
      paramFormats : access Interfaces.C.int;
      resultFormat : Interfaces.C.int)
     return A_PGRESULT_T;
   --  libpq-fe.h:308

   function PQPREPARE
     (conn       : A_PGCONN_T;
      stmtName   : Interfaces.C.Strings.chars_ptr;
      query      : Interfaces.C.Strings.chars_ptr;
      nParams    : Interfaces.C.int;
      paramTypes : access Oid)
     return A_PGRESULT_T;
   --  libpq-fe.h:316

   function PQEXECPREPARED
     (conn         : A_PGCONN_T;
      stmtName     : Interfaces.C.Strings.chars_ptr;
      nParams      : Interfaces.C.int;
      paramValues  : access Interfaces.C.Strings.chars_ptr;
      paramLengths : access Interfaces.C.int;
      paramFormats : access Interfaces.C.int;
      resultFormat : Interfaces.C.int)
     return A_PGRESULT_T;
   --  libpq-fe.h:319

   ----------------------------------------
   --  Interface for multiple-result or  --
   --  asynchronous queries              --
   ----------------------------------------

   function PQSENDQUERY
     (conn  : A_PGCONN_T;
      query : Interfaces.C.Strings.chars_ptr)
     return Interfaces.C.int;
   --  libpq-fe.h:328

   function PQSENDQUERYPARAMS
     (conn         : A_PGCONN_T;
      command      : Interfaces.C.Strings.chars_ptr;
      nParams      : Interfaces.C.int;
      paramTypes   : access Oid;
      paramValues  : access Interfaces.C.Strings.chars_ptr;
      paramLengths : access Interfaces.C.int;
      paramFormats : access Interfaces.C.int;
      resultFormat : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:329

   function PQSENDPREPARE
     (conn       : A_PGCONN_T;
      stmtName   : Interfaces.C.Strings.chars_ptr;
      query      : Interfaces.C.Strings.chars_ptr;
      nParams    : Interfaces.C.int;
      paramTypes : access Oid)
     return Interfaces.C.int;
   --  libpq-fe.h:337

   function PQSENDQUERYPREPARED
     (conn         : A_PGCONN_T;
      stmtName     : Interfaces.C.Strings.chars_ptr;
      nParams      : Interfaces.C.int;
      paramValues  : access Interfaces.C.Strings.chars_ptr;
      paramLengths : access Interfaces.C.int;
      paramFormats : access Interfaces.C.int;
      resultFormat : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:340

   function PQGETRESULT
     (conn : A_PGCONN_T)
     return A_PGRESULT_T;
   --  libpq-fe.h:347

   ---------------------------------------------------
   --  Routines for managing an asynchronous query  --
   ---------------------------------------------------

   function PQISBUSY
     (conn : A_PGCONN_T)
     return Interfaces.C.int;
   --  libpq-fe.h:350

   function PQCONSUMEINPUT
     (conn : A_PGCONN_T)
     return Interfaces.C.int;
   --  libpq-fe.h:351


   function PQNOTIFIES
     (conn : A_PGCONN_T)
     return A_PGNOTIFY_T;
   --  libpq-fe.h:354
   --  LISTEN/NOTIFY support

   --------------------------------
   --  Routines for copy in/out  --
   --------------------------------
   function PQPUTCOPYDATA
     (conn   : A_PGCONN_T;
      buffer : Interfaces.C.Strings.chars_ptr;
      nbytes : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:357

   function PQPUTCOPYEND
     (conn     : A_PGCONN_T;
      errormsg : Interfaces.C.Strings.chars_ptr)
     return Interfaces.C.int;
   --  libpq-fe.h:358

   function PQGETCOPYDATA
     (conn   : A_PGCONN_T;
      buffer : access Interfaces.C.Strings.chars_ptr;
      async  : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:359

   -------------------------------------------
   --  Deprecated routines for copy in/out  --
   -------------------------------------------
   function PQGETLINE
     (conn   : A_PGCONN_T;
      string : Interfaces.C.Strings.chars_ptr;
      length : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:362

   function PQPUTLINE
     (conn   : A_PGCONN_T;
      string : Interfaces.C.Strings.chars_ptr)
     return Interfaces.C.int;
   --  libpq-fe.h:363

   function PQGETLINEASYNC
     (conn    : A_PGCONN_T;
      buffer  : Interfaces.C.Strings.chars_ptr;
      bufsize : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:364

   function PQPUTNBYTES
     (conn   : A_PGCONN_T;
      buffer : Interfaces.C.Strings.chars_ptr;
      nbytes : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:365

   function PQENDCOPY
     (conn : A_PGCONN_T)
     return Interfaces.C.int;
   --  libpq-fe.h:366

   ----------------------------------------------------------
   --  Set blocking/nonblocking connection to the backend  --
   ----------------------------------------------------------

   function PQSETNONBLOCKING
     (conn : A_PGCONN_T;
      arg  : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:369

   function PQISNONBLOCKING
     (conn : A_PGCONN_T)
     return Interfaces.C.int;
   --  libpq-fe.h:370


   function PQISTHREADSAFE
     return Interfaces.C.int;
   --  libpq-fe.h:371

   function PQFLUSH
     (conn : A_PGCONN_T)
     return Interfaces.C.int;
   --  libpq-fe.h:374
   --  Force the write buffer to be written (or at least try)

   function PQFN
     (conn          : A_PGCONN_T;
      fnid          : Interfaces.C.int;
      result_buf    : access Interfaces.C.int;
      result_len    : access Interfaces.C.int;
      result_is_int : Interfaces.C.int;
      args          : A_PQARGBLOCK_T;
      nargs         : Interfaces.C.int)
     return A_PGRESULT_T;
   --  libpq-fe.h:380
   --  "Fast path" interface ---  not really recommended for application use


   -----------------------------------------------
   --  Accessor functions for PGresult objects  --
   -----------------------------------------------

   function PQRESULTSTATUS
     (res : A_PGRESULT_T)
     return ExecStatusType;
   --  libpq-fe.h:389

   function PQRESSTATUS
     (status : ExecStatusType)
     return Interfaces.C.Strings.chars_ptr;
   --  libpq-fe.h:390

   function PQRESULTERRORMESSAGE
     (res : A_PGRESULT_T)
     return Interfaces.C.Strings.chars_ptr;
   --  libpq-fe.h:391

   function PQRESULTERRORFIELD
     (res       : A_PGRESULT_T;
      fieldcode : Interfaces.C.int)
     return Interfaces.C.Strings.chars_ptr;
   --  libpq-fe.h:392

   function PQNTUPLES
     (res : A_PGRESULT_T)
     return Interfaces.C.int;
   --  libpq-fe.h:393

   function PQNFIELDS
     (res : A_PGRESULT_T)
     return Interfaces.C.int;
   --  libpq-fe.h:394

   function PQBINARYTUPLES
     (res : A_PGRESULT_T)
     return Interfaces.C.int;
   --  libpq-fe.h:395

   function PQFNAME
     (res       : A_PGRESULT_T;
      field_num : Interfaces.C.int)
     return Interfaces.C.Strings.chars_ptr;
   --  libpq-fe.h:396

   function PQFNUMBER
     (res        : A_PGRESULT_T;
      field_name : Interfaces.C.Strings.chars_ptr)
     return Interfaces.C.int;
   --  libpq-fe.h:397

   function PQFTABLE
     (res       : A_PGRESULT_T;
      field_num : Interfaces.C.int)
     return Oid;
   --  libpq-fe.h:398

   function PQFTABLECOL
     (res       : A_PGRESULT_T;
      field_num : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:399

   function PQFFORMAT
     (res       : A_PGRESULT_T;
      field_num : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:400

   function PQFTYPE
     (res       : A_PGRESULT_T;
      field_num : Interfaces.C.int)
     return Oid;
   --  libpq-fe.h:401

   function PQFSIZE
     (res       : A_PGRESULT_T;
      field_num : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:402

   function PQFMOD
     (res       : A_PGRESULT_T;
      field_num : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:403

   function PQCMDSTATUS
     (res : A_PGRESULT_T)
     return Interfaces.C.Strings.chars_ptr;
   --  libpq-fe.h:404

   function PQOIDSTATUS
     (res : A_PGRESULT_T)
     return Interfaces.C.Strings.chars_ptr;
   --  libpq-fe.h:405

   function PQOIDVALUE
     (res : A_PGRESULT_T)
     return Oid;
   --  libpq-fe.h:406

   function PQCMDTUPLES
     (res : A_PGRESULT_T)
     return Interfaces.C.Strings.chars_ptr;
   --  libpq-fe.h:407

   function PQGETVALUE
     (res       : A_PGRESULT_T;
      tup_num   : Interfaces.C.int;
      field_num : Interfaces.C.int)
     return Interfaces.C.Strings.chars_ptr;
   --  libpq-fe.h:408

   function PQGETLENGTH
     (res       : A_PGRESULT_T;
      tup_num   : Interfaces.C.int;
      field_num : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:409

   function PQGETISNULL
     (res       : A_PGRESULT_T;
      tup_num   : Interfaces.C.int;
      field_num : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:410

   function PQNPARAMS
     (res : A_PGRESULT_T)
     return Interfaces.C.int;
   --  libpq-fe.h:411

   function PQPARAMTYPE
     (res       : A_PGRESULT_T;
      param_num : Interfaces.C.int)
     return Oid;
   --  libpq-fe.h:412


   ------------------------------------------------
   --  Describe prepared statements and portals  --
   ------------------------------------------------

   function PQDESCRIBEPREPARED
     (conn : A_PGCONN_T;
      stmt : Interfaces.C.Strings.chars_ptr)
     return A_PGRESULT_T;
   --  libpq-fe.h:415

   function PQDESCRIBEPORTAL
     (conn   : A_PGCONN_T;
      portal : Interfaces.C.Strings.chars_ptr)
     return A_PGRESULT_T;
   --  libpq-fe.h:416

   function PQSENDDESCRIBEPREPARED
     (conn : A_PGCONN_T;
      stmt : Interfaces.C.Strings.chars_ptr)
     return Interfaces.C.int;
   --  libpq-fe.h:417

   function PQSENDDESCRIBEPORTAL
     (conn   : A_PGCONN_T;
      portal : Interfaces.C.Strings.chars_ptr)
     return Interfaces.C.int;
   --  libpq-fe.h:418


   procedure PQCLEAR
     (res : A_PGRESULT_T);
   --  libpq-fe.h:421
   --  Delete a PGresult

   procedure PQFREEMEM
     (ptr : System.Address);
   --  libpq-fe.h:424
   --  For freeing other alloc'd results, such as PGnotify structs

   function PQMAKEEMPTYPGRESULT
     (conn   : A_PGCONN_T;
      status : ExecStatusType)
     return A_PGRESULT_T;
   --  libpq-fe.h:438
   --  Make an empty PGresult with given status (some apps find this useful).
   --  If conn is not NULL and status indicates an error,
   --  conn's errorMessage is copied.

   ----------------------------------------------------
   --  Quoting strings before inclusion in queries.  --
   ----------------------------------------------------

   function PQESCAPESTRINGCONN
     (conn   : A_PGCONN_T;
      to     : Interfaces.C.Strings.chars_ptr;
      from   : Interfaces.C.Strings.chars_ptr;
      length : Interfaces.C.size_t;
      error  : access Interfaces.C.int)
     return Interfaces.C.size_t;
   --  libpq-fe.h:442

   function PQESCAPEBYTEACONN
     (conn        : A_PGCONN_T;
      from        : access Interfaces.C.unsigned_char;
      from_length : Interfaces.C.size_t;
      to_length   : access Interfaces.C.size_t)
     return access Interfaces.C.unsigned_char;
   --  libpq-fe.h:445
   --

   function PQUNESCAPEBYTEA
     (strtext   : access Interfaces.C.unsigned_char;
      retbuflen : access Interfaces.C.size_t)
     return access Interfaces.C.unsigned_char;
   --  libpq-fe.h:448

   -----------------------------------
   --  These forms are deprecated!  --
   -----------------------------------

   function PQESCAPESTRING
     (to     : Interfaces.C.Strings.chars_ptr;
      from   : Interfaces.C.Strings.chars_ptr;
      length : Interfaces.C.size_t)
     return Interfaces.C.size_t;
   --  libpq-fe.h:452

   function PQESCAPEBYTEA
     (from        : access Interfaces.C.unsigned_char;
      from_length : Interfaces.C.size_t;
      to_length   : access Interfaces.C.size_t)
     return access Interfaces.C.unsigned_char;
   --  libpq-fe.h:453

   --
   --

   -------------------------
   --  printing routines  --
   -------------------------

   procedure PQPRINT
     (fout : Interfaces.C_Streams.FILEs;
      res  : in A_PGRESULT_T;
      ps   : in A_PQPRINTOPT_T);
   --  libpq-fe.h:461

   ------------------------------------
   --  really old printing routines  --
   ------------------------------------

   procedure PQDISPLAYTUPLES
     (res         : A_PGRESULT_T;
      fp          : Interfaces.C_Streams.FILEs;
      fillAlign   : Interfaces.C.int;
      fieldSep    : Interfaces.C.Strings.chars_ptr;
      printHeader : Interfaces.C.int;
      quiet       : Interfaces.C.int);
   --  libpq-fe.h:469

   procedure PQPRINTTUPLES
     (res          : A_PGRESULT_T;
      fout         : Interfaces.C_Streams.FILEs;
      printAttName : Interfaces.C.int;
      terseOutput  : Interfaces.C.int;
      width        : Interfaces.C.int);
   --  libpq-fe.h:477

   ------------------------------------
   --  Large-object access routines  --
   ------------------------------------

   function lo_Open
     (conn   : A_PGCONN_T;
      lobjId : Oid;
      mode   : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:487

   function lo_Close
     (conn : A_PGCONN_T;
      fd   : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:488

   function lo_Read
     (conn : A_PGCONN_T;
      fd   : Interfaces.C.int;
      buf  : Interfaces.C.Strings.chars_ptr;
      len  : Interfaces.C.size_t)
     return Interfaces.C.int;
   --  libpq-fe.h:489

   function lo_Write
     (conn : A_PGCONN_T;
      fd   : Interfaces.C.int;
      buf  : Interfaces.C.Strings.chars_ptr;
      len  : Interfaces.C.size_t)
     return Interfaces.C.int;
   --  libpq-fe.h:490

   function lo_Lseek
     (conn   : A_PGCONN_T;
      fd     : Interfaces.C.int;
      offset : Interfaces.C.int;
      whence : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:491

   function lo_Creat
     (conn : A_PGCONN_T;
      mode : Interfaces.C.int)
     return Oid;
   --  libpq-fe.h:492

   function lo_Create
     (conn   : A_PGCONN_T;
      lobjId : Oid)
     return Oid;
   --  libpq-fe.h:493

   function lo_Tell
     (conn : A_PGCONN_T;
      fd   : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:494

   function lo_Truncate
     (conn : A_PGCONN_T;
      fd   : Interfaces.C.int;
      len  : Interfaces.C.size_t)
     return Interfaces.C.int;
   --  libpq-fe.h:495

   function lo_Unlink
     (conn   : A_PGCONN_T;
      lobjId : Oid)
     return Interfaces.C.int;
   --  libpq-fe.h:496

   function lo_Import
     (conn     : A_PGCONN_T;
      filename : Interfaces.C.Strings.chars_ptr)
     return Oid;
   --  libpq-fe.h:497

   function lo_Export
     (conn     : A_PGCONN_T;
      lobjId   : Oid;
      filename : Interfaces.C.Strings.chars_ptr)
     return Interfaces.C.int;
   --  libpq-fe.h:498

   --------------------
   --  in fe-misc.c  --
   --------------------

   function PQMBLEN
     (s        : Interfaces.C.Strings.chars_ptr;
      encoding : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:503
   --  Determine length of multibyte encoded char at *s

   function PQDSPLEN
     (s        : Interfaces.C.Strings.chars_ptr;
      encoding : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:506
   --  Determine display length of multibyte encoded char at *s

   function PQENV2ENCODING
     return Interfaces.C.int;
   --  libpq-fe.h:509
   --  Get encoding id from environment variable PGCLIENTENCODING

   function PQENCRYPTPASSWORD
     (passwd : Interfaces.C.Strings.chars_ptr;
      user   : Interfaces.C.Strings.chars_ptr)
     return Interfaces.C.Strings.chars_ptr;
   --  libpq-fe.h:513

   --------------------
   --  in fe-auth.c  --
   --------------------

   function pg_char_to_encoding
     (name : Interfaces.C.Strings.chars_ptr)
     return Interfaces.C.int;
   --  libpq-fe.h:517

   ---------------------
   --  in encnames.c  --
   ---------------------

   function pg_encoding_to_char
     (encoding : Interfaces.C.int)
     return Interfaces.C.Strings.chars_ptr;
   --  libpq-fe.h:518

   function pg_valid_server_encoding_id
     (encoding : Interfaces.C.int)
     return Interfaces.C.int;
   --  libpq-fe.h:519

private

   pragma Import (C, PQCONNECTSTART, "PQconnectStart");
   --  libpq-fe.h:204

   pragma Import (C, PQCONNECTPOLL, "PQconnectPoll");
   --  libpq-fe.h:205

   pragma Import (C, PQCONNECTDB, "PQconnectdb");
   --  libpq-fe.h:208

   pragma Import (C, PQSETDBLOGIN, "PQsetdbLogin");
   --  libpq-fe.h:209

   pragma Import (C, PQFINISH, "PQfinish");
   --  libpq-fe.h:218

   pragma Import (C, PQCONNDEFAULTS, "PQconndefaults");
   --  libpq-fe.h:221

   pragma Import (C, PQCONNINFOFREE, "PQconninfoFree");
   --  libpq-fe.h:224

   pragma Import (C, PQRESETSTART, "PQresetStart");
   --  libpq-fe.h:231

   pragma Import (C, PQRESETPOLL, "PQresetPoll");
   --  libpq-fe.h:232

   pragma Import (C, PQRESET, "PQreset");
   --  libpq-fe.h:235

   pragma Import (C, PQGETCANCEL, "PQgetCancel");
   --  libpq-fe.h:238

   pragma Import (C, PQFREECANCEL, "PQfreeCancel");
   --  libpq-fe.h:241

   pragma Import (C, PQCANCEL, "PQcancel");
   --  libpq-fe.h:244

   pragma Import (C, PQREQUESTCANCEL, "PQrequestCancel");
   --  libpq-fe.h:247

   pragma Import (C, PQDB, "PQdb");
   --  libpq-fe.h:250

   pragma Import (C, PQUSER, "PQuser");
   --  libpq-fe.h:251

   pragma Import (C, PQPASS, "PQpass");
   --  libpq-fe.h:252

   pragma Import (C, PQHOST, "PQhost");
   --  libpq-fe.h:253

   pragma Import (C, PQPORT, "PQport");
   --  libpq-fe.h:254

   pragma Import (C, PQTTY, "PQtty");
   --  libpq-fe.h:255

   pragma Import (C, PQOPTIONS, "PQoptions");
   --  libpq-fe.h:256

   pragma Import (C, PQSTATUS, "PQstatus");
   --  libpq-fe.h:257

   pragma Import (C, PQTRANSACTIONSTATUS, "PQtransactionStatus");
   --  libpq-fe.h:258

   pragma Import (C, PQPARAMETERSTATUS, "PQparameterStatus");
   --  libpq-fe.h:259

   pragma Import (C, PQPROTOCOLVERSION, "PQprotocolVersion");
   --  libpq-fe.h:261

   pragma Import (C, PQSERVERVERSION, "PQserverVersion");
   --  libpq-fe.h:262

   pragma Import (C, PQERRORMESSAGE, "PQerrorMessage");
   --  libpq-fe.h:263

   pragma Import (C, PQSOCKET, "PQsocket");
   --  libpq-fe.h:264

   pragma Import (C, PQBACKENDPID, "PQbackendPID");
   --  libpq-fe.h:265

   pragma Import (C, PQCONNECTIONNEEDSPASSWORD, "PQconnectionNeedsPassword");
   --  libpq-fe.h:266

   pragma Import (C, PQCONNECTIONUSEDPASSWORD, "PQconnectionUsedPassword");
   --  libpq-fe.h:267

   pragma Import (C, PQCLIENTENCODING, "PQclientEncoding");
   --  libpq-fe.h:268

   pragma Import (C, PQSETCLIENTENCODING, "PQsetClientEncoding");
   --  libpq-fe.h:269

   pragma Import (C, PQGETSSL, "PQgetssl");
   --  libpq-fe.h:273

   pragma Import (C, PQINITSSL, "PQinitSSL");
   --  libpq-fe.h:276

   pragma Import (C, PQSETERRORVERBOSITY, "PQsetErrorVerbosity");
   --  libpq-fe.h:279

   pragma Import (C, PQTRACE, "PQtrace");
   --  libpq-fe.h:282

   pragma Import (C, PQUNTRACE, "PQuntrace");
   --  libpq-fe.h:283

   pragma Import (C, PQSETNOTICERECEIVER, "PQsetNoticeReceiver");
   --  libpq-fe.h:286

   pragma Import (C, PQSETNOTICEPROCESSOR, "PQsetNoticeProcessor");
   --  libpq-fe.h:289

   pragma Import (C, PQREGISTERTHREADLOCK, "PQregisterThreadLock");
   --  libpq-fe.h:302

   pragma Import (C, PQEXEC, "PQexec");
   --  libpq-fe.h:307

   pragma Import (C, PQEXECPARAMS, "PQexecParams");
   --  libpq-fe.h:308

   pragma Import (C, PQPREPARE, "PQprepare");
   --  libpq-fe.h:316

   pragma Import (C, PQEXECPREPARED, "PQexecPrepared");
   --  libpq-fe.h:319

   pragma Import (C, PQSENDQUERY, "PQsendQuery");
   --  libpq-fe.h:328

   pragma Import (C, PQSENDQUERYPARAMS, "PQsendQueryParams");
   --  libpq-fe.h:329

   pragma Import (C, PQSENDPREPARE, "PQsendPrepare");
   --  libpq-fe.h:337

   pragma Import (C, PQSENDQUERYPREPARED, "PQsendQueryPrepared");
   --  libpq-fe.h:340

   pragma Import (C, PQGETRESULT, "PQgetResult");
   --  libpq-fe.h:347

   pragma Import (C, PQISBUSY, "PQisBusy");
   --  libpq-fe.h:350

   pragma Import (C, PQCONSUMEINPUT, "PQconsumeInput");
   --  libpq-fe.h:351

   pragma Import (C, PQNOTIFIES, "PQnotifies");
   --  libpq-fe.h:354

   pragma Import (C, PQPUTCOPYDATA, "PQputCopyData");
   --  libpq-fe.h:357

   pragma Import (C, PQPUTCOPYEND, "PQputCopyEnd");
   --  libpq-fe.h:358

   pragma Import (C, PQGETCOPYDATA, "PQgetCopyData");
   --  libpq-fe.h:359

   pragma Import (C, PQGETLINE, "PQgetline");
   --  libpq-fe.h:362

   pragma Import (C, PQPUTLINE, "PQputline");
   --  libpq-fe.h:363

   pragma Import (C, PQGETLINEASYNC, "PQgetlineAsync");
   --  libpq-fe.h:364

   pragma Import (C, PQPUTNBYTES, "PQputnbytes");
   --  libpq-fe.h:365

   pragma Import (C, PQENDCOPY, "PQendcopy");
   --  libpq-fe.h:366

   pragma Import (C, PQSETNONBLOCKING, "PQsetnonblocking");
   --  libpq-fe.h:369

   pragma Import (C, PQISNONBLOCKING, "PQisnonblocking");
   --  libpq-fe.h:370

   pragma Import (C, PQISTHREADSAFE, "PQisthreadsafe");
   --  libpq-fe.h:371

   pragma Import (C, PQFLUSH, "PQflush");
   --  libpq-fe.h:374

   pragma Import (C, PQFN, "PQfn");
   --  libpq-fe.h:380

   pragma Import (C, PQRESULTSTATUS, "PQresultStatus");
   --  libpq-fe.h:389

   pragma Import (C, PQRESSTATUS, "PQresStatus");
   --  libpq-fe.h:390

   pragma Import (C, PQRESULTERRORMESSAGE, "PQresultErrorMessage");
   --  libpq-fe.h:391

   pragma Import (C, PQRESULTERRORFIELD, "PQresultErrorField");
   --  libpq-fe.h:392

   pragma Import (C, PQNTUPLES, "PQntuples");
   --  libpq-fe.h:393

   pragma Import (C, PQNFIELDS, "PQnfields");
   --  libpq-fe.h:394

   pragma Import (C, PQBINARYTUPLES, "PQbinaryTuples");
   --  libpq-fe.h:395

   pragma Import (C, PQFNAME, "PQfname");
   --  libpq-fe.h:396

   pragma Import (C, PQFNUMBER, "PQfnumber");
   --  libpq-fe.h:397

   pragma Import (C, PQFTABLE, "PQftable");
   --  libpq-fe.h:398

   pragma Import (C, PQFTABLECOL, "PQftablecol");
   --  libpq-fe.h:399

   pragma Import (C, PQFFORMAT, "PQfformat");
   --  libpq-fe.h:400

   pragma Import (C, PQFTYPE, "PQftype");
   --  libpq-fe.h:401

   pragma Import (C, PQFSIZE, "PQfsize");
   --  libpq-fe.h:402

   pragma Import (C, PQFMOD, "PQfmod");
   --  libpq-fe.h:403

   pragma Import (C, PQCMDSTATUS, "PQcmdStatus");
   --  libpq-fe.h:404

   pragma Import (C, PQOIDSTATUS, "PQoidStatus");
   --  libpq-fe.h:405

   pragma Import (C, PQOIDVALUE, "PQoidValue");
   --  libpq-fe.h:406

   pragma Import (C, PQCMDTUPLES, "PQcmdTuples");
   --  libpq-fe.h:407

   pragma Import (C, PQGETVALUE, "PQgetvalue");
   --  libpq-fe.h:408

   pragma Import (C, PQGETLENGTH, "PQgetlength");
   --  libpq-fe.h:409

   pragma Import (C, PQGETISNULL, "PQgetisnull");
   --  libpq-fe.h:410

   pragma Import (C, PQNPARAMS, "PQnparams");
   --  libpq-fe.h:411

   pragma Import (C, PQPARAMTYPE, "PQparamtype");
   --  libpq-fe.h:412

   pragma Import (C, PQDESCRIBEPREPARED, "PQdescribePrepared");
   --  libpq-fe.h:415

   pragma Import (C, PQDESCRIBEPORTAL, "PQdescribePortal");
   --  libpq-fe.h:416

   pragma Import (C, PQSENDDESCRIBEPREPARED, "PQsendDescribePrepared");
   --  libpq-fe.h:417

   pragma Import (C, PQSENDDESCRIBEPORTAL, "PQsendDescribePortal");
   --  libpq-fe.h:418

   pragma Import (C, PQCLEAR, "PQclear");
   --  libpq-fe.h:421

   pragma Import (C, PQFREEMEM, "PQfreemem");
   --  libpq-fe.h:424

   pragma Import (C, PQMAKEEMPTYPGRESULT, "PQmakeEmptyPGresult");
   --  libpq-fe.h:438

   pragma Import (C, PQESCAPESTRINGCONN, "PQescapeStringConn");
   --  libpq-fe.h:442

   pragma Import (C, PQESCAPEBYTEACONN, "PQescapeByteaConn");
   --  libpq-fe.h:445

   pragma Import (C, PQUNESCAPEBYTEA, "PQunescapeBytea");
   --  libpq-fe.h:448

   pragma Import (C, PQESCAPESTRING, "PQescapeString");
   --  libpq-fe.h:452

   pragma Import (C, PQESCAPEBYTEA, "PQescapeBytea");
   --  libpq-fe.h:453

   pragma Import (C, PQPRINT, "PQprint");
   --  libpq-fe.h:461

   pragma Import (C, PQDISPLAYTUPLES, "PQdisplayTuples");
   --  libpq-fe.h:469

   pragma Import (C, PQPRINTTUPLES, "PQprintTuples");
   --  libpq-fe.h:477

   pragma Import (C, lo_Open, "lo_open");
   --  libpq-fe.h:487

   pragma Import (C, lo_Close, "lo_close");
   --  libpq-fe.h:488

   pragma Import (C, lo_Read, "lo_read");
   --  libpq-fe.h:489

   pragma Import (C, lo_Write, "lo_write");
   --  libpq-fe.h:490

   pragma Import (C, lo_Lseek, "lo_lseek");
   --  libpq-fe.h:491

   pragma Import (C, lo_Creat, "lo_creat");
   --  libpq-fe.h:492

   pragma Import (C, lo_Create, "lo_create");
   --  libpq-fe.h:493

   pragma Import (C, lo_Tell, "lo_tell");
   --  libpq-fe.h:494

   pragma Import (C, lo_Truncate, "lo_truncate");
   --  libpq-fe.h:495

   pragma Import (C, lo_Unlink, "lo_unlink");
   --  libpq-fe.h:496

   pragma Import (C, lo_Import, "lo_import");
   --  libpq-fe.h:497

   pragma Import (C, lo_Export, "lo_export");
   --  libpq-fe.h:498

   pragma Import (C, PQMBLEN, "PQmblen");
   --  libpq-fe.h:503

   pragma Import (C, PQDSPLEN, "PQdsplen");
   --  libpq-fe.h:506

   pragma Import (C, PQENV2ENCODING, "PQenv2encoding");
   --  libpq-fe.h:509

   pragma Import (C, PQENCRYPTPASSWORD, "PQencryptPassword");
   --  libpq-fe.h:513

   pragma Import (C, pg_char_to_encoding, "pg_char_to_encoding");
   --  libpq-fe.h:517

   pragma Import (C, pg_encoding_to_char, "pg_encoding_to_char");
   --  libpq-fe.h:518

   pragma Import (C, pg_valid_server_encoding_id,
                  "pg_valid_server_encoding_id");
   --  libpq-fe.h:519

end Backlit.Thin;
