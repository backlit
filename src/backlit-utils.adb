with Ada.Strings.Fixed;
with Ada.Characters.Handling;

package body Backlit.Utils is

   function Escape (S : String) return String
   is
      Result : String (1 .. S'Length * 2);
      Last   : Natural := 0;
   begin
      for I in S'Range loop
         if S (I) = ''' then
            Last := Last + 1;
            Result (Last) := '\';
         end if;
         Last := Last + 1;
         Result (Last) := S (I);
      end loop;
      return ''' & Result (1 .. Last) & ''';
   end Escape;

   function To_Lower (Source : String) return String
   is
   begin
      return Ada.Characters.Handling.To_Lower (Source);
   end To_Lower;

   function Trim (Source : String) return String
   is
   begin
      return Ada.Strings.Fixed.Trim (Source, Ada.Strings.Both);
   end Trim;

   function Boolean_Str  (S : String) return String is
      Tmp : String := Trim (To_Lower (S));
   begin
      if Tmp = "true" then return "True";
      elsif Tmp = "t" then return "True";
      elsif Tmp = "yes" then return "True";
      elsif Tmp = "y" then return "True";
      elsif Tmp = "false" then return "False";
      elsif Tmp = "f" then return "False";
      elsif Tmp = "no" then return "False";
      elsif Tmp = "n" then return "False";
      end if;
      return S;
   end Boolean_Str;

   function Trim_Left_Zero (S: String) return String
   is
      use Ada.Strings.Fixed;
      Tmp : String := Trim (S, Ada.Strings.Both);
      Z: String (Tmp'First .. Tmp'Last) := (others => '0');
      C: Natural := 0;
      L : Natural := Tmp'Last;
   begin
      loop
         exit when L < 1;
         C := Index (S, Z (Z'First  .. L));
         if C = 1 then
            if L = S'Length then
               return S (S'First + L - 1 .. S'Last);
            else
               return S (S'First + L .. S'Last);
            end if;
         end if;
         L := L - 1;
      end loop;
      return S;
   end Trim_Left_Zero;

end Backlit.Utils;
