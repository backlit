package Backlit.Utils is


   function Escape (S : String) return String;
   -- Quote a string

   function Trim (Source : String) return String;
   -- Trim both sides of a string

   function To_Lower (Source : String) return String;

   function Boolean_Str  (S : String) return String;
   -- return "True" if S = (true|yes|y|t)
   -- return "False" if S = (false|no|n|f)
   -- not case sensitive. otherwise return S unchanged

   function Trim_Left_Zero (S: String) return String;
   -- remove any 0 at the left of a string
   --  example :00010 => 10; 000 => 0; 0.10 => .10

end Backlit.Utils;
