------------------------------------------------------------------------------
--                                                                          --
-- Copyright 2008, Ali Bendriss                                             --
--                                                                          --
-- This file is part of Backlit.                                            --
--                                                                          --
-- Backlit is free software: you can redistribute it and/or modify it under --
-- the terms of the GNU Lesser General Public License as published by       --
-- the Free Software Foundation, either version 3 of the License, or        --
-- (at your option) any later version.                                      --
--                                                                          --
-- Backlit is distributed in the hope that it will be useful,               --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of           --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the             --
-- GNU Lesser General Public License for more details.                      --
--                                                                          --
-- You should have received a copy of the GNU Lesser General Public License --
-- along with Backlit. If not, see <http://www.gnu.org/licenses/>.          --
--                                                                          --
------------------------------------------------------------------------------

package Backlit is

   pragma Pure;

   Error, Query_Error : exception;

end Backlit;
